.PHONY: native
native: $(NATIVE_TARGET)

.PHONY: native/compile_commands.json
native/compile_commands.json: Makefile # $(SRB_DEPS) $(NATIVE_SRC)
	# Clean native dependencies:
	$(MAKE) -w -C $(SOURCE_DIR)/dependencies clean
	# Build all native dependencies (all architectures on Apple Silicon)
	$(MAKE) -w -C $(SOURCE_DIR) dependencies/compiled
	# Clean our stuff
	$(MAKE) -w -C $(SOURCE_DIR) native/clean
	# Compile and feed into compiledb
	$(MAKE) -w -C $(SOURCE_DIR) bin/srbutil$(SUFFIX) | $(COMPILEDB) --full-path -d $(SOURCE_DIR) -m -o $@

$(NATIVE_TARGET): $(NATIVE_OBJ) | native/lib_$(ARCH)/pkgconfig
	$(AR) rcs $@ $^
	sed 's/VERSION/$(SOURCE_VERSION)/;s|PREFIX|$(NATIVE_ROOT)|;s|LIBDIR|$(NATIVE_ROOT)/lib_$(ARCH)|;s|LINKLIBS|$(NATIVE_LINKLIBS)|' '$(NATIVE_ROOT)/libsrb_native.pc' > '$(NATIVE_ROOT)/lib_$(ARCH)/pkgconfig/libsrb_native.pc'

native/obj_$(ARCH)/%.o: native/src/%.c $(NATIVE_HEADERS) | dependencies/compiled_$(ARCH) native/obj_$(ARCH)
	$(CC) $(NATIVE_CFLAGS) '-DSOURCE_VERSION="$(SOURCE_VERSION)$(SUFFIX)"' -c $< -o $@

.PHONY: native/clang-format
native/clang-format:
	cd native && clang-format --verbose -i **/*.c **/*.h

.PHONY: native/check-format
native/check-format:
	cd native && clang-format --verbose -Werror --dry-run **/*.c **/*.h

.PHONY: native/clean
native/clean:
	$(RM) -r native/lib_*/ native/obj_*/
	$(RM) $(TEMP_DIR)/srbutil_* bin/srbutil*

native/ldd: bin/srbutil$(SUFFIX)
	$(LDD) $^

native/obj_$(ARCH) native/lib_$(ARCH)/pkgconfig:
	mkdir -p $@

.DELETE_ON_ERROR: bin/srbutil$(SUFFIX)
ifeq ($(MACOS_ROSETTA),no)
bin/srbutil$(SUFFIX): $(TEMP_DIR)/srbutil_$(ARCH) | bin
	cp $^ $@
	$(CODESIGN)
else
bin/srbutil$(SUFFIX): $(TEMP_DIR)/srbutil_x86_64 $(TEMP_DIR)/srbutil_arm64 | bin
	lipo -create -output $@ $^
	@#cp '$@' '$@_unstripped'
ifneq ($(DEBUG),)
	strip $@
endif
	$(CODESIGN)

  ifeq ($(MACOS_ROSETTA),active)

.DELETE_ON_ERROR: $(TEMP_DIR)/srbutil_arm64
$(TEMP_DIR)/srbutil_arm64:
	arch -arch arm64 make -w $@
  else
.DELETE_ON_ERROR: $(TEMP_DIR)/srbutil_x86_64
$(TEMP_DIR)/srbutil_x86_64:
	arch -arch x86_64 make -w $@
  endif
endif

.DELETE_ON_ERROR: $(TEMP_DIR)/srbutil_$(ARCH)
$(TEMP_DIR)/srbutil_$(ARCH): native/srbutil/main.c $(NATIVE_TARGET)
	CFLAGS='' $(CC) -o $@ $^ $(NATIVE_CFLAGS) $(SRBUTIL_LDFLAGS)
	@#cp '$@' '$@_unstripped'
ifneq ($(DEBUG),)
	strip $@
endif
###############################

.PHONY: fuzz
fuzz: $(FUZZES)

# $FUZZES: $FUZZES_BIN
.PHONY: fuzz/%
fuzz/%: $(TEMP_DIR)/fuzz/%
	@echo Running $@ for $(FUZZ_TIME) seconds...
	ASAN_OPTIONS=detect_leaks=1 $^ -max_total_time=$(FUZZ_TIME) > $(TEMP_DIR)/$@.log 2>&1

# $FUZZES_BIN
$(TEMP_DIR)/fuzz/%: native/fuzz/%.cpp $(NATIVE_TARGET)
	@mkdir -p "$(TEMP_DIR)/fuzz"
	CFLAGS='' $(CLANG) -o $@ -o $@ $^ $(NATIVE_CFLAGS) -flto $(SRBUTIL_LDFLAGS) -g -O1 -fsanitize=fuzzer,signed-integer-overflow,address,leak
