package common

type Decoder interface {
	FillBuffer(targetBuffersize uint, bufferCount uint) (bufferchain chan *AudioBuffer)
	GetInfo() (filename string, rate int, channels int, sample_type interface{}, decoderFormatName string, resampled bool, dsd uint16)
	SentBuffers() uint
	Close()
	TryStop()
	GetMQA() MQAInformation
}
