package common

import "sync"

type AudioBuffer struct {
	Data *[]byte
	*sync.Mutex
}

func NewAudioBuffer(size uint) AudioBuffer {
	buf := make([]byte, size, size)
	audio := AudioBuffer{
		&buf,
		&sync.Mutex{},
	}
	return audio
}

func (ab *AudioBuffer) Free() {
	*(ab.Data) = nil
}
