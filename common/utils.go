package common

import (
	"errors"
	"github.com/gordonklaus/portaudio"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	timetrack "gitlab.com/T4cC0re/time-track"
	"reflect"
	"time"
)

var shuttingDown bool

func SetShutDown(state bool) {
	shuttingDown = state
}

func GetShutDown() bool {
	return shuttingDown
}

func BufferBytesPerSecond(rate int, channels int, depth uint8) (size uint) {
	defer timetrack.TimeTrack(time.Now())
	size = uint(rate * channels * int(depth/8))

	return
}

func GetBits(sampleType interface{}) uint8 {
	defer timetrack.TimeTrack(time.Now())
	if _, ok := sampleType.(portaudio.Int24); ok {
		return 24
	} else {
		return uint8(reflect.TypeOf(sampleType).Bits())
	}
}

func AssertNotErr(err error) {
	defer timetrack.TimeTrack(time.Now())
	if err != nil {
		if err == portaudio.StreamIsStopped {
			// This happens, when Stop is called on the stream when it's already closed.
			if shuttingDown {
				return
			}
			log.WithField("shuttingDown", shuttingDown).Errorf("PortAudio Error: '%s'", err.Error())
		}
		if err2, ok := err.(portaudio.UnanticipatedHostError); ok {
			// This happens, when Abort is called on the stream while writing.
			if shuttingDown {
				return
			}
			log.WithField("shuttingDown", shuttingDown).Errorf("PortAudio Error: '%s', API: %v, Code: %d", err2.Text, err2.HostApiType.String(), err2.Code)
			if err2.Code == -77 {
			}
		}
		log.WithField("shuttingDown", shuttingDown).Error(err)
		panic(err)
	}
}

func AssertNotNil(v interface{}, errorString string) {
	defer timetrack.TimeTrack(time.Now())
	if v == nil || (reflect.ValueOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).IsNil()) {
		log.Fatal(errors.New(errorString))
	}
}
