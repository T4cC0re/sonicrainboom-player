/**
 * MQA is a registered trademark of MQA Limited. This code does not intend to infringe trademarks.
 *
 * Furthermore this code does not allow to create, alter or "unfold" a stream. This code merely identifies the existence
 * of MQA in an audio stream and extracts the original sample rate as well as the provenance.
 *
 * To unfold a MQA stream, the user has to use this in conjunction with a MQA Full Decoder. This does not perform
 * the first unfold (MQA Core).
 */

package common

type MQAInformation struct {
	OriginalSampleRate uint32
	IsMQA              bool
	Pending            bool // Whether samples have finished analysis
	IsMQAStudio        bool // true if Mastered according to MQA Studio ("blue light")
	Provenance         uint8
	Depth              uint8
}

////// REPLACED WITH NATIVE CODE //////
//
//func (mqa *MQAInformation) String() string {
//	os.Exit(1)
//	switch true {
//	case mqa.Pending:
//		return "analyzing"
//	case mqa.IsMQAStudio:
//		return fmt.Sprintf("MQA Studio (%d-bit %d Hz)", mqa.Depth, mqa.OriginalSampleRate)
//	case mqa.IsMQA:
//		return fmt.Sprintf("MQA (%d-bit %d Hz)", mqa.Depth, mqa.OriginalSampleRate)
//	default:
//		return "no MQA"
//	}
//}
//
//func (mqa *MQAInformation) SetOriginalSampleRate(code uint8) {
//	os.Exit(1)
//	defer timetrack.TimeTrack(time.Now())
//
//	var base uint32
//	var multiplier uint32
//
//	/*
//	 * The least significant bit signifies the used base (44100 Hz (0) or 48000 Hz (1)
//	 * The next 3 bits are the multiplier. This needs to be unmangled, and in case of DSD
//	 * multiplied by 2 itself.
//	 * In the end the base and the multiplier will produce the original sample rate.
//	 */
//	if code&1 == 1 {
//		base = 48000
//	} else {
//		base = 44100
//	}
//
//	multiplier = 1 << (((code >> 3) & 1) | (((code >> 2) & 1) << 1) | (((code >> 1) & 1) << 2))
//
//	/**
//	 * At this point we reached a higher sample rate than 705600 Hz, which signalizes, that
//	 * the MQA file was encoded from a DSD source. We double the multiplied to get to DSD
//	 * sample rates.
//	 */
//	if multiplier > 16 {
//		multiplier *= 2
//	}
//
//	/**
//	 * The code seems to have 5 bits. It's unclear what this bit does. But since sample rates
//	 * calculated the "old" way only go up to DSD256, the 5th bit seems to be a flag to
//	 * 1. switch to a different calculation altogether
//	 * 2. double the rate
//	 * Number 2 seems to be a viable option to implement until we know more.
//	 */
//	if code&0b00010000 == 0b00010000 {
//		// DSD 512? Maybe?
//		multiplier *= 2
//	}
//
//	mqa.OriginalSampleRate = base * multiplier
//}
//
//func (mqa *MQAInformation) AnalyzeSamples(accumulator *uint64, samples uint, buffer *[]byte, depth uint8, channels int) bool {
//	os.Exit(1)
//	defer timetrack.TimeTrack(time.Now())
//
//	if depth < 16 || depth > 24 {
//		log.Error("AnalyzeSamples called on non 16/24-bit stream")
//		return false
//	}
//	if channels != 2 {
//		log.Error("AnalyzeSamples called on stream with other than 2 channels")
//		return false
//	}
//	if accumulator == nil {
//		log.Error("AnalyzeSamples called without an accumulator")
//		return false
//	}
//	if buffer == nil {
//		log.Error("AnalyzeSamples called without a buffer")
//		return false
//	}
//	if len(*buffer) < int(samples)*int((depth)/8)*channels {
//		log.Error("AnalyzeSamples called with a too small buffer")
//		return false
//	}
//
//	var sample uint64 = 0
//	for sample < uint64(samples) {
//		// TODO: Better bounds checks for MQA markers near the end of a frame/buffer
//		*accumulator |= hiddenBitFromSample(depth, sample, buffer)
//
//		if *accumulator == 0b101111100000010010011000110010001000 { // We arrived at the MQA marker
//			log.Debugf("MQA marker found in samples at sample %d (of %d in buffer)", sample, samples)
//
//			// Original Sample Rate
//			{
//				var codedSampleRate uint8 = 0
//				var sampleOffset uintptr = 3
//				for sampleOffset < 8 {
//					codedSampleRate |= uint8(hiddenBitFromSample(depth, sample+uint64(sampleOffset), buffer)) << (6 - sampleOffset)
//					sampleOffset++
//				}
//				mqa.SetOriginalSampleRate(codedSampleRate)
//			}
//
//			// Provenance Code
//			{
//				var provenance uint8 = 0
//				var sampleOffset uintptr = 29
//				for sampleOffset < 34 {
//					provenance |= uint8(hiddenBitFromSample(depth, sample+uint64(sampleOffset), buffer)) << (33 - sampleOffset)
//					sampleOffset++
//				}
//				mqa.Provenance = provenance
//				mqa.IsMQAStudio = provenance > 8
//			}
//
//			mqa.Depth = depth
//			mqa.IsMQA = true
//			mqa.Pending = false
//
//			return true
//		}
//
//		*accumulator = (*accumulator << 1) & 0xFFFFFFFFF
//		sample++
//	}
//
//	return false
//}
//
//func hiddenBitFromSample(depth uint8, sample uint64, buffer *[]byte) (xor uint64) {
//	os.Exit(1)
//	defer timetrack.TimeTrack(time.Now())
//
//	var bufferOffset uint64 = sample * uint64((depth)/8) * 2
//	var bufferAddress uintptr = (*reflect.SliceHeader)(unsafe.Pointer(buffer)).Data
//
//	left := *(*uint32)(unsafe.Pointer(bufferAddress + uintptr(bufferOffset)))
//	right := *(*uint32)(unsafe.Pointer(bufferAddress + uintptr(bufferOffset) + uintptr((depth)/8)))
//
//	xor = ((uint64(left) ^ uint64(right)) >> (depth - 16)) & 1
//
//	return
//}
