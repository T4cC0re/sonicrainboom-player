package player

import (
	. "gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	"runtime"
)

func (player *Player) Dummy() {
f:
	for {
		select {
		case audio, valid := <-player.bufferchain:
			player.PrintStats()
			player.buffers_consumed++
			log.Infof("got next buffer...")
			if !valid {
				if !GetShutDown() {
					// Don't warn when shutting down
					log.Warn("invalid buffer...")
				}
				break f
			}
			if audio == nil || len(*audio.Data) == 0 {
				close(player.bufferchain)
				break f
			}

			log.Warnf("Dropping audio buffer...")
			audio.Lock()
			audio.Unlock()
			audio.Free()
		default:
		}
	}
	runtime.GC()
	player.PrintStats()
}
