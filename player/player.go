package player

import (
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/gordonklaus/portaudio"
	. "gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	timetrack "gitlab.com/T4cC0re/time-track"
	"os"
	"reflect"
	"runtime"
	"strings"
	"time"
)

type Player struct {
	audioBuffer16     []int16
	audioBuffer24     []portaudio.Int24
	audioBuffer32     []int32
	audioBuffer32f    []float32
	audioBuffer64f    []float64
	bufferchain       chan *AudioBuffer
	stream            *portaudio.Stream
	consume_stop      chan struct{}
	consume_done      chan struct{}
	buffers_consumed  uint
	underflows        uint
	sampleType        interface{}
	filename          string
	decoderFormatName string
	rate              int
	channels          int
	decoder           *Decoder
	buffercount       uint
	buffersize        uint
	resampled         bool
	dsd               uint16
	DACSwitchTime     time.Duration
	AuthToken         string
}

func (player *Player) DeInit() {
	defer timetrack.TimeTrack(time.Now())
	AssertNotErr(portaudio.Terminate())
}

func (player *Player) Init() {
	defer timetrack.TimeTrack(time.Now())
	player.audioBuffer16 = make([]int16, 0)           // Used by portaudio, and in consumeBufferchain
	player.audioBuffer24 = make([]portaudio.Int24, 0) // Used by portaudio, and in consumeBufferchain
	player.audioBuffer32 = make([]int32, 0)           // Used by portaudio, and in consumeBufferchain
	player.audioBuffer32f = make([]float32, 0)        // Used by portaudio, and in consumeBufferchain
	player.audioBuffer64f = make([]float64, 0)        // Used by portaudio, and in consumeBufferchain
	AssertNotErr(portaudio.Initialize())
}

func (player *Player) Close() {
	defer timetrack.TimeTrack(time.Now())
	SetShutDown(true)
	if player.decoder != nil {
		(*player.decoder).Close()
	}
	if player.stream != nil {
		_ = (*player.stream).Close()
	}
	player.stream = nil
	player.decoder = nil
	player.buffers_consumed = 0
	player.buffercount = 0
}

func (player *Player) InitSong(device *Device, decoder *Decoder, buffercount uint, buffersizemultiplier uint, dump string) (err error) {
	defer timetrack.TimeTrack(time.Now())
	SetShutDown(false)
	AssertNotNil(device, "device must not be nil")
	AssertNotNil(decoder, "decoder must not be nil")
	player.decoder = decoder
	player.filename, player.rate, player.channels, player.sampleType, player.decoderFormatName, player.resampled, player.dsd = (*player.decoder).GetInfo()

	depth := GetBits(player.sampleType)

	log.Infof("DSD? %d, Depth: %d, type %s, resampled?: %t", player.dsd, depth, reflect.TypeOf(player.sampleType).Name(), player.resampled)

	if player.dsd > 0 {
		log.Warnf("DSD%d Detected. This is currently unsupported. Weird stuff might happen", player.dsd)
	}

	player.buffersize = BufferBytesPerSecond(player.rate, player.channels, depth)
	player.buffercount = buffercount

	if strings.HasPrefix(player.filename, "http") {
		player.buffercount *= 16
	}

	if buffersizemultiplier > 0 {
		player.buffersize *= buffersizemultiplier
	}

	p := portaudio.LowLatencyParameters(nil, device.DeviceInfo)
	p.Output.Channels = player.channels
	p.Flags |= portaudio.DitherOff | portaudio.ClipOff
	p.SampleRate = float64(player.rate)
	//p.FramesPerBuffer = buffersize

	switch player.sampleType.(type) {
	case int16:
		player.stream, err = portaudio.OpenStream(p, &player.audioBuffer16)
	case portaudio.Int24:
		player.stream, err = portaudio.OpenStream(p, &player.audioBuffer24)
	case int32:
		player.stream, err = portaudio.OpenStream(p, &player.audioBuffer32)
	case float32:
		player.stream, err = portaudio.OpenStream(p, &player.audioBuffer32f)
	case float64:
		player.stream, err = portaudio.OpenStream(p, &player.audioBuffer64f)
	default:
		err = fmt.Errorf("unsuppored type %v", reflect.TypeOf(player.sampleType).Name())
		player.stream = nil
	}

	AssertNotErr(err)
	AssertNotNil(player.stream, "stream must not be nil")

	log.Debugf("%+v", player.stream.Info())

	if device.HostApi.Type != portaudio.CoreAudio {
		// This does not work on macOS
		var frames int
		frames, err = player.stream.AvailableToWrite()
		for (frames <= 0) && (err == nil) {
			log.Infof("Stream not ready...")
			frames, err = player.stream.AvailableToWrite()
		}
	}

	player.consume_stop = make(chan struct{}, 0)
	player.consume_done = make(chan struct{}, 0)

	player.bufferchain = (*player.decoder).FillBuffer(player.buffersize, player.buffercount)

	time.Sleep(player.DACSwitchTime)

	if dump != "" {
		player.createDumpChain(dump)
	}

	return
}

func (player *Player) Play(stop chan struct{}) error {
	go player.consumeBufferchain()

	var cancel = make(chan struct{}, 0)

	go func() {
		select {
		case <-stop:
			log.Info("Received Interrupt")
			SetShutDown(true)
			player.stream.Abort()
			log.Info("setting fill_stop")
			(*player.decoder).TryStop()
			log.Info("setting consume_stop")
			close(player.consume_stop)

			log.Info("draining bufferchain")
			for {
				val := <-player.bufferchain
				if val == nil {
					break
				}
			}
			log.Info("drained bufferchain")
			close(cancel)
		}
	}()

	// Just wait for either to finish
	select {
	case <-player.consume_done:
	case <-cancel:
	}
	return nil
}

func (player *Player) PrintStats() {
	defer timetrack.TimeTrack(time.Now())
	if player.decoder != nil {
		buffers_sent := (*player.decoder).SentBuffers()

		log.Infof("MQA: %s", (*player.decoder).GetMQA())
		log.Infof(
			"Buffer state: %d consumed, %d sent, %d available, %d max_buffers, total buffered: %s",
			player.buffers_consumed,
			buffers_sent,
			buffers_sent-player.buffers_consumed,
			player.buffercount,
			humanize.IBytes(uint64((buffers_sent-player.buffers_consumed+1)*player.buffersize)),
		)
		log.Infof("Underflows\t: %d", player.underflows)
	}

	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	log.Infof("HeapAlloc\t: %s", humanize.IBytes(m.HeapAlloc))
	log.Infof("HeapSys\t: %s", humanize.IBytes(m.HeapSys))
	log.Infof("HeapIdle\t: %s", humanize.IBytes(m.HeapIdle))
	log.Infof("HeapInuse\t: %s", humanize.IBytes(m.HeapInuse))
	log.Infof("HeapReleased\t: %s", humanize.IBytes(m.HeapReleased))
	log.Infof("StackInuse\t: %s", humanize.IBytes(m.StackInuse))
	log.Infof("StackSys\t: %s", humanize.IBytes(m.StackSys))
	log.Infof("Sys\t\t: %s", humanize.IBytes(m.Sys))
	log.Infof("NumGC\t: %d\n", m.NumGC)
	log.Infof("NumGC\t: %d\n", m.NumGC)
	log.Infof("Lookups\t: %d\n", m.Lookups)
	log.Infof("Mallocs\t: %d\n", m.Mallocs)
	log.Infof("HeapObjects\t: %d\n", m.HeapObjects)
	log.Infof("Frees\t: %d\n", m.Frees)
}

func containsWithFor(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func (player *Player) CheckCapability(decoder *Decoder, device Device) (ok bool) {
	defer timetrack.TimeTrack(time.Now())
	_, rate, channels, sampleType, _, _, dsd := (*decoder).GetInfo()
	ok = true

	if dsd > 0 {
		log.Warnf("DSD%d Detected. This is currently unsupported. Failing Compat check", dsd)
		return false
	}

	if channels > 2 {
		log.Warnf("Channel count %d unsupported", channels)
		return false
	}

	depth := GetBits(sampleType)
	if depth%8 != 0 || depth < 16 || depth > 32 {
		log.Warnf("depth %d-bit unsupported", depth)
		return false
	}

	switch sampleType.(type) {
	default:
		log.Warnf("type %s unsppported", reflect.TypeOf(sampleType).Name())
		return false
	case int16:
		ok = containsWithFor(device.S16, rate)
	case portaudio.Int24:
		ok = containsWithFor(device.S24, rate)
	case int32:
		ok = containsWithFor(device.S32, rate)
	case float32:
		ok = containsWithFor(device.F32, rate)
	case float64:
		ok = containsWithFor(device.F64, rate)
	}
	if !ok {
		log.Warnf("rate %d unsppported for %s", rate, reflect.TypeOf(sampleType).Name())
	}

	return
}

func (player *Player) createDumpChain(dump string) {

	dump = fmt.Sprintf("%s.%s_%d_%d.pcm", dump, player.decoderFormatName, player.rate, player.channels)

	file, err := os.OpenFile(dump, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	if err != nil {
		log.Error("Could not create dump file")
		return
	}

	real_chain := player.bufferchain
	player.bufferchain = make(chan *AudioBuffer, player.buffercount)

	go func() {
		for {
			select {
			case audio, valid := <-real_chain:
				log.Infof("dumping next buffer...")
				if !valid {
					file.Close()
					close(player.bufferchain)
					return
				}
				if audio == nil || len(*audio.Data) == 0 {
					file.Close()
					close(real_chain)
					close(player.bufferchain)
					return
				}

				file.Write(*audio.Data)
				file.Sync()

				player.bufferchain <- audio
			}
		}
	}()
}
