//+build linux

package player

import (
	"flag"
)

func init() {
	flag.BoolVar(&usePulse, "pulse", usePulse, "Use PulseAudio (recommended for Desktop use, NOT bit-perfect, thus unsuitable for MQA or DSD)")
}
