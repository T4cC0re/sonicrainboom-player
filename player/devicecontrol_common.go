package player

import (
	"errors"
	"flag"
	"fmt"
	"github.com/gordonklaus/portaudio"
	. "gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	timetrack "gitlab.com/T4cC0re/time-track"
	"strings"
	"time"
)

var deviceName string

var usePulse bool = false
var listDevices bool = false

func init() {
	dev, err := portaudio.DefaultOutputDevice()
	//var def string = "default"
	var def string = ""
	_ = def
	if err == nil {
		def = dev.Name
	}
	//flag.StringVar(&deviceName, "device", def, "Device name to play on (does nothing if using PulseAudio on Linux)")
	flag.BoolVar(&listDevices, "listdevices", listDevices, "Show a list of detected devices")
	// usePulse is only added as a flag on Linux. Thus it is only in `devicecontrol_linux.go`
}

type Device struct {
	*portaudio.DeviceInfo
	Capabilities
}

type Capabilities struct {
	S16      []int
	S24      []int
	S32      []int
	F32      []int
	F64      []int
	DSD      []int
	detected bool
}

func (d *Device) GetCapabilities() string {
	if !d.detected {
		d.DetectCapabilities()
	}

	var caps []string

	for _, rate := range d.S16 {
		caps = append(caps, fmt.Sprintf("16/%d", rate))
	}
	for _, rate := range d.S24 {
		caps = append(caps, fmt.Sprintf("24/%d", rate))
	}
	for _, rate := range d.S32 {
		caps = append(caps, fmt.Sprintf("32/%d", rate))
	}
	for _, rate := range d.F32 {
		caps = append(caps, fmt.Sprintf("f32/%d", rate))
	}
	for _, rate := range d.F64 {
		caps = append(caps, fmt.Sprintf("f64/%d", rate))
	}
	for _, rate := range d.DSD {
		caps = append(caps, fmt.Sprintf("DSD%d", rate))
	}

	return strings.Join(caps, " ")
}

func (d *Device) DetectCapabilities() {
	defer timetrack.TimeTrack(time.Now())

	if d.detected {
		return
	}

	var audioBuffer16 []int16
	var audioBuffer24 []portaudio.Int24
	var audioBuffer32 []int32
	var audioBuffer32f []float32
	var audioBuffer64f []float64

	var rates = []int{44100, 48000, 88200, 96000, 176400, 192000, 352800, 384000, 705600, 768000}

	for _, rate := range rates {
		sp := portaudio.StreamParameters{
			Input: portaudio.StreamDeviceParameters{},
			Output: portaudio.StreamDeviceParameters{
				Device:   d.DeviceInfo,
				Channels: 2,
			},
			SampleRate: float64(rate),
		}

		if err := portaudio.IsFormatSupported(sp, &audioBuffer16); err == nil {
			d.S16 = append(d.S16, rate)
		}
		if err := portaudio.IsFormatSupported(sp, &audioBuffer24); err == nil {
			d.S24 = append(d.S24, rate)
		}
		if err := portaudio.IsFormatSupported(sp, &audioBuffer32); err == nil {
			d.S32 = append(d.S32, rate)
		}
		if err := portaudio.IsFormatSupported(sp, &audioBuffer32f); err == nil {
			d.F32 = append(d.F32, rate)
		}
		if err := portaudio.IsFormatSupported(sp, &audioBuffer64f); err == nil {
			d.F64 = append(d.F64, rate)
		}
	}

	d.detected = true
}

func (player *Player) ListDevices() bool {
	if !listDevices {
		return false
	}
	defer timetrack.TimeTrack(time.Now())
	devs, err := portaudio.Devices()
	AssertNotErr(err)

	var devices []Device

	for _, dev := range devs {
		d := Device{DeviceInfo: dev}
		d.DetectCapabilities()
		devices = append(devices, d)
	}
	for _, d := range devices {
		var prefix = ""
		if d.HostApi.DefaultOutputDevice.Name == d.Name {
			prefix = ">"
		} else {
			prefix = " "
		}
		log.Infof("%s %s", prefix, d.Name)
		log.Infof("    Capabilities: %s", d.GetCapabilities())
	}
	return true
}

func (player *Player) GetDevice() (device *portaudio.DeviceInfo) {
	defer timetrack.TimeTrack(time.Now())
	devs, err := portaudio.Devices()
	AssertNotErr(err)

	if usePulse {
		deviceName = "pulse"
	}

	for _, dev := range devs {
		if dev == nil {
			continue
		}
		if dev.Name == deviceName {
			device = dev
		}
	}

	if device == nil {
		if usePulse {
			AssertNotErr(errors.New("pulseaudio unavailable"))
		} else {
			AssertNotErr(errors.New("device not found"))
		}
	}
	return device
}
