package player

import "C"
import (
	"github.com/gordonklaus/portaudio"
	. "gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	"gitlab.com/T4cC0re/sonicrainboom-player/native"
	timetrack "gitlab.com/T4cC0re/time-track"

	"fmt"
	"reflect"
	"runtime"
	"runtime/debug"
	"strings"
	"time"
	"unsafe"
)

func (player *Player) consumeBufferchain() {
	AssertNotErr(player.stream.Start())
	defer func() {
		if _, err := player.stream.AvailableToWrite(); err == nil {
			_ = player.stream.Stop()
		}
		close(player.consume_done)
	}()
	defer func() {
		if err, ok := recover().(error); ok {
			if GetShutDown() {
				log.Infof("shutting down...")
				return
			}
			log.Error("consumeBufferchain paniced...")
			SetShutDown(true)
			AssertNotErr(err)
		}
	}()
	for {
		if player.buffers_consumed%5 == 0 {
			go func() {
				runtime.GC()
				debug.FreeOSMemory()
			}()
		}
		select {
		case <-player.consume_stop:
			SetShutDown(true)
			return
		case audio, valid := <-player.bufferchain:
			player.buffers_consumed++
			if !valid {
				return
			}
			if audio == nil || audio.Data == nil || len(*audio.Data) == 0 {
				close(player.bufferchain)
				return
			}
			log.Debugf("got next buffer...")
			err := player.playBuffer(audio)
			if err != nil {
				if strings.HasPrefix(err.Error(), "Output underflowed") {
					player.underflows++
					log.Error(err)
					continue
				} else {
					log.Fatal(err)
				}
			} else {
				log.Debugf("played buffer")
			}
		}
	}
}

func (player *Player) playBuffer(audio *AudioBuffer) (err error) {
	defer timetrack.TimeTrack(time.Now())
	var sh *reflect.SliceHeader

	if audio.Data == nil {
		return nil
	}

	audio.Mutex.Lock()
	defer audio.Mutex.Unlock()

	switch player.sampleType.(type) {
	case int16:
		sh = (*reflect.SliceHeader)(unsafe.Pointer(&player.audioBuffer16))
		sh.Len = len(*audio.Data) / 2
		log.Debugf("new buffer of []int16 @ %d bytes / %d samples @ 2 bytes", len(player.audioBuffer16), len(player.audioBuffer16)/2)
	case portaudio.Int24:
		sh = (*reflect.SliceHeader)(unsafe.Pointer(&player.audioBuffer24))
		sh.Len = len(*audio.Data) / 3
		log.Debugf("new buffer of []int24 @ %d bytes / %d samples @ 3 bytes", len(player.audioBuffer24), len(player.audioBuffer24)/3)
	case int32:
		sh = (*reflect.SliceHeader)(unsafe.Pointer(&player.audioBuffer32))
		sh.Len = len(*audio.Data) / 4
		log.Debugf("new buffer of []int32 @ %d bytes / %d samples @ 4 bytes", len(player.audioBuffer32), len(player.audioBuffer32)/4)
	case float32:
		sh = (*reflect.SliceHeader)(unsafe.Pointer(&player.audioBuffer32f))
		sh.Len = len(*audio.Data) / 4
		log.Debugf("new buffer of []float32 @ %d bytes / %d samples @ 4 bytes", len(player.audioBuffer32f), len(player.audioBuffer32f)/4)
	case float64:
		sh = (*reflect.SliceHeader)(unsafe.Pointer(&player.audioBuffer64f))
		sh.Len = len(*audio.Data) / 8
		log.Debugf("new buffer of []float64 @ %d bytes / %d samples @ 8 bytes", len(player.audioBuffer64f), len(player.audioBuffer64f)/8)
	default:
		return fmt.Errorf("unsuppored type %v", reflect.TypeOf(player.sampleType).Name())
	}

	log.Debugf("data ptr: 0x%x", (*reflect.SliceHeader)(unsafe.Pointer(audio.Data)).Data)
	sh.Data = uintptr(native.ByteSliceToPtr(audio.Data))
	sh.Cap = sh.Len

	err = player.stream.Write()
	audio.Free()
	audio = nil
	return
}
