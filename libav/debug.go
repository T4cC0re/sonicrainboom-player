//+build debug

package libav

/*
	#cgo pkg-config: --static libavutil-srb
	#include <libavutil/log.h>
*/
import "C"

func init() {
	C.av_log_set_level(56)
}
