package libav

/*
	#cgo pkg-config: --static libavutil-srb
	#include <libavutil/opt.h>
	#include <libswresample/swresample.h>
	#include <stdlib.h>
*/
import "C"

import (
	"gitlab.com/T4cC0re/goav/avutil"
	"gitlab.com/T4cC0re/goav/swresample"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	timetrack "gitlab.com/T4cC0re/time-track"
	"os"
	"time"
	"unsafe"
)

const DITHER_DEFAULT int = SWR_DITHER_NS_HIGH_SHIBATA

var checkSupported func(string) bool = int_checkSupported

func int_checkSupported(resampler string) bool {
	ctx := swresample.SwrAlloc()
	if ctx == nil {
		// go tests will not touch this, as we cannot fake a failed malloc
		log.Errorf("Could not allocate context to check '%s' resampler", resampler)
		return false
	}
	defer C.free(unsafe.Pointer(ctx))

	// Mock minimal context settings
	ctx.SwrAllocSetOpts(AV_CH_LAYOUT_STEREO, AV_SAMPLE_FMT_S16, 44100, AV_CH_LAYOUT_STEREO, AV_SAMPLE_FMT_S16, 44100, 0, 0)

	opt_name := C.CString("resampler")
	opt_val := C.CString(resampler)
	res := int(C.av_opt_set(unsafe.Pointer(ctx), opt_name, opt_val, 0))
	C.free(unsafe.Pointer(opt_name))
	C.free(unsafe.Pointer(opt_val))

	if res != 0 {
		return false
	}

	// Grab the old loglevel, make libav quiet, do the init and reset the log level
	oldLog := avutil.AvLogGetLevel()
	avutil.AvLogSetLevel(avutil.AV_LOG_QUIET)
	res = ctx.SwrInit()
	avutil.AvLogSetLevel(oldLog)

	return res == 0
}

func (lav LibAV) getDitherSettings() (dither int, resampler int) {
	defer timetrack.TimeTrack(time.Now())
	switch lav.dither {
	case "none":
		dither = SWR_DITHER_NONE
	case "rectangular":
		dither = SWR_DITHER_RECTANGULAR
	case "triangular":
		dither = SWR_DITHER_TRIANGULAR
	case "triangular_hp":
		dither = SWR_DITHER_TRIANGULAR_HIGHPASS
	case "shibata":
		dither = SWR_DITHER_NS_SHIBATA
	case "low_shibata":
		dither = SWR_DITHER_NS_LOW_SHIBATA
	case "high_shibata":
		dither = SWR_DITHER_NS_HIGH_SHIBATA
	case "lipshitz":
		dither = SWR_DITHER_NS_LIPSHITZ
	case "f_weighted":
		dither = SWR_DITHER_NS_F_WEIGHTED
	case "modified_e_weighted":
		dither = SWR_DITHER_NS_MODIFIED_E_WEIGHTED
	case "improved_e_weighted":
		dither = SWR_DITHER_NS_IMPROVED_E_WEIGHTED
	default:
		log.Warnf("Dither: '%s' is unknown.", lav.dither)
		dither = DITHER_DEFAULT // if input is == 24-bit that might be changed
	}

	if lav.override_depth == 24 && dither >= SWR_DITHER_NS {
		log.Warn("Dither: Noise shaping dither on 24-bit audio is unsupported. Using 'Triangular High Pass'")
		dither = SWR_DITHER_TRIANGULAR_HIGHPASS
	}

	switch dither {
	case SWR_DITHER_NONE:
		log.Infof("Dither: None")
	case SWR_DITHER_RECTANGULAR:
		log.Infof("Dither: Rectangular")
	case SWR_DITHER_TRIANGULAR:
		log.Infof("Dither: Triangular")
	case SWR_DITHER_TRIANGULAR_HIGHPASS:
		log.Infof("Dither: Triangular High Pass")
	case SWR_DITHER_NS_SHIBATA:
		log.Infof("Dither: Shibata Noise Shaping")
	case SWR_DITHER_NS_LOW_SHIBATA:
		log.Infof("Dither: Shibata Noise Shaping (low)")
	case SWR_DITHER_NS_HIGH_SHIBATA:
		log.Infof("Dither: Shibata Noise Shaping (high)")
	case SWR_DITHER_NS_LIPSHITZ:
		log.Infof("Dither: Lipshitz Noise Shaping")
	case SWR_DITHER_NS_F_WEIGHTED:
		log.Infof("Dither: F weighted Noise Shaping")
	case SWR_DITHER_NS_MODIFIED_E_WEIGHTED:
		log.Infof("Dither: modified E weighted Noise Shaping")
	case SWR_DITHER_NS_IMPROVED_E_WEIGHTED:
		log.Infof("Dither: improved E weighted Noise Shaping")
	}

	switch lav.resampler {
	case "soxr":
		// TODO: Include proper build from source. Leave the check regardless
		if checkSupported("soxr") {
			log.Infof("Resampler: SoX Resampler")
			resampler = SWR_ENGINE_SOXR
			break
		}
		log.Warnf("Resampler: 'SoX Resampler' selected, but not available.")
		fallthrough
	case "swr":
		log.Infof("Resampler: native SW Resampler")
		resampler = SWR_ENGINE_SWR
	default:
		log.Warnf("Resampler: '%s' is unknown. Using 'native SW Resampler'", lav.resampler)
		resampler = SWR_ENGINE_SWR
	}

	return dither, resampler
}

func (lav LibAV) setupSwr(frame *avutil.Frame /*, workFrame *avutil.Frame*/) (err error) {
	defer timetrack.TimeTrack(time.Now())

	if frame == nil {
		return os.ErrInvalid
	}
	c_frame := (*C.struct_AVFrame)(unsafe.Pointer(frame))
	//if workFrame != nil {
	//	c_workFrame := (*C.struct_AVFrame)(unsafe.Pointer(workFrame))
	//
	//	(*c_workFrame).format = C.int32_t(lav.override_sample_fmt)
	//	(*c_workFrame).sample_rate = C.int32_t(lav.override_sample_rate)
	//	(*c_workFrame).channels = C.int32_t(lav.override_channels)
	//	(*c_workFrame).channel_layout = C.uint64_t(lav.override_channel_layout)
	//
	//	log.Debugf("(*c_workFrame).channel_layout: %d, (*c_workFrame).channels: %d, (*c_workFrame).sample_rate: %d", (*c_workFrame).channel_layout, (*c_workFrame).channels, (*c_workFrame).sample_rate)
	//} else {
	//	log.Debugf("setupSwr called without workFrame")
	//}

	dither, resampler := lav.getDitherSettings()
	//err = lav.setDitherSettings(dither, resampler)
	err = lav.Native.SetDitherSettings(dither, resampler)
	if err != nil {
		return err
	}

	opt_name := C.CString("in_channel_count")
	err = avutil.ErrorFromCode(int(C.av_opt_set_int(unsafe.Pointer(lav.swrCtx), opt_name, C.int64_t((*c_frame).channels), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("out_channel_count")
	err = avutil.ErrorFromCode(int(C.av_opt_set_int(unsafe.Pointer(lav.swrCtx), opt_name, C.int64_t(lav.override_channels), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("in_channel_layout")
	err = avutil.ErrorFromCode(int(C.av_opt_set_int(unsafe.Pointer(lav.swrCtx), opt_name, C.int64_t((*c_frame).channel_layout), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("out_channel_layout")
	err = avutil.ErrorFromCode(int(C.av_opt_set_int(unsafe.Pointer(lav.swrCtx), opt_name, C.int64_t(lav.override_channel_layout), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("in_sample_rate")
	err = avutil.ErrorFromCode(int(C.av_opt_set_int(unsafe.Pointer(lav.swrCtx), opt_name, C.int64_t((*c_frame).sample_rate), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("out_sample_rate")
	err = avutil.ErrorFromCode(int(C.av_opt_set_int(unsafe.Pointer(lav.swrCtx), opt_name, C.int64_t(lav.override_sample_rate), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("in_sample_fmt")
	err = avutil.ErrorFromCode(int(C.av_opt_set_sample_fmt(unsafe.Pointer(lav.swrCtx), opt_name, (C.enum_AVSampleFormat)((*c_frame).format), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	opt_name = C.CString("out_sample_fmt")
	err = avutil.ErrorFromCode(int(C.av_opt_set_sample_fmt(unsafe.Pointer(lav.swrCtx), opt_name, (C.enum_AVSampleFormat)(lav.override_sample_fmt), 0)))
	C.free(unsafe.Pointer(opt_name))
	if err != nil {
		return err
	}
	return nil
}
