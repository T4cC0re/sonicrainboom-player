package libav

import (
	"gitlab.com/T4cC0re/goav/avutil"
	"gitlab.com/T4cC0re/goav/swresample"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	"gitlab.com/T4cC0re/sonicrainboom-player/native"
	"os"
	"testing"
	"unsafe"
)

func init() {
	log.SetLevel(log.LOG_QUIET)
}

func TestGetDitherSettings(t *testing.T) {
	defer func() {
		checkSupported = int_checkSupported // Restore var after test
	}()
	lav := LibAV{
		dither:    "",
		resampler: "",
	}

	expectations := []struct {
		depth              uint8
		inDither           string
		inResampler        string
		outDither          int
		outResampler       int
		resamplerSupported bool
	}{
		/// region Defaults and edge-cases
		{
			// Defaults
			outDither:    DITHER_DEFAULT,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// Want soxr, and is compiled in
			inResampler:        "soxr",
			outDither:          DITHER_DEFAULT,
			outResampler:       SWR_ENGINE_SOXR,
			resamplerSupported: true,
		},
		{
			// Want soxr, and is NOT compiled in
			inResampler:        "soxr",
			outDither:          DITHER_DEFAULT,
			outResampler:       SWR_ENGINE_SWR,
			resamplerSupported: false,
		},
		{
			// Want other unsupported resampler
			inResampler:        "unsupported",
			outDither:          DITHER_DEFAULT,
			outResampler:       SWR_ENGINE_SWR,
			resamplerSupported: false,
		},
		{
			// Defaults @ 24-bit
			depth:        24,
			outDither:    SWR_DITHER_TRIANGULAR_HIGHPASS,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// shibata selected @ 24-bit -> triangular_hp
			depth:        24,
			inDither:     "shibata",
			inResampler:  "swr",
			outDither:    SWR_DITHER_TRIANGULAR_HIGHPASS,
			outResampler: SWR_ENGINE_SWR,
		},
		/// endregion
		/// region Dither value mappings:
		{
			// none
			inDither:     "none",
			outDither:    SWR_DITHER_NONE,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// rectangular
			inDither:     "rectangular",
			outDither:    SWR_DITHER_RECTANGULAR,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// triangular
			inDither:     "triangular",
			outDither:    SWR_DITHER_TRIANGULAR,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// triangular_hp
			inDither:     "triangular_hp",
			outDither:    SWR_DITHER_TRIANGULAR_HIGHPASS,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// shibata
			inDither:     "shibata",
			outDither:    SWR_DITHER_NS_SHIBATA,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// low_shibata
			inDither:     "low_shibata",
			outDither:    SWR_DITHER_NS_LOW_SHIBATA,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// high_shibata
			inDither:     "high_shibata",
			outDither:    SWR_DITHER_NS_HIGH_SHIBATA,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// lipshitz
			inDither:     "lipshitz",
			outDither:    SWR_DITHER_NS_LIPSHITZ,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// f_weighted
			inDither:     "f_weighted",
			outDither:    SWR_DITHER_NS_F_WEIGHTED,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// modified_e_weighted
			inDither:     "modified_e_weighted",
			outDither:    SWR_DITHER_NS_MODIFIED_E_WEIGHTED,
			outResampler: SWR_ENGINE_SWR,
		},
		{
			// improved_e_weighted
			inDither:     "improved_e_weighted",
			outDither:    SWR_DITHER_NS_IMPROVED_E_WEIGHTED,
			outResampler: SWR_ENGINE_SWR,
		},
		/// endregion
		/// region Resample value mappings: (asuming compiled in)
		{
			// soxr
			inResampler:        "soxr",
			outDither:          DITHER_DEFAULT,
			outResampler:       SWR_ENGINE_SOXR,
			resamplerSupported: true,
		},
		{
			// swr
			inResampler:        "swr",
			outDither:          DITHER_DEFAULT,
			outResampler:       SWR_ENGINE_SWR,
			resamplerSupported: true,
		},
		/// endregion
	}

	for _, expected := range expectations {
		lav.override_depth = expected.depth
		lav.dither = expected.inDither
		lav.resampler = expected.inResampler

		checkSupported = func(s string) bool {
			return expected.resamplerSupported
		}

		dither, resampler := lav.getDitherSettings()

		if dither != expected.outDither {
			goto fail
		}
		if resampler != expected.outResampler {
			goto fail
		}
		continue
	fail:
		t.Fail()
		t.Logf("Test FAIL wanted: %+v, have: dither %d, resampler: %d", expected, dither, resampler)
	}
	if t.Failed() {
		t.FailNow()
	}
}

func TestCheckSupported_OK(t *testing.T) {
	defer func() {
		if err := recover(); err != nil {
			t.FailNow()
		}
	}()

	if !checkSupported("swr") { // Definitely exists
		t.FailNow()
	}
}
func TestCheckSupported_UnsupportedResampler(t *testing.T) {
	defer avutil.AvLogSetLevel(avutil.AvLogGetLevel())
	avutil.AvLogSetLevel(avutil.AV_LOG_QUIET)
	defer func() {
		if err := recover(); err != nil {
			t.FailNow()
		}
	}()

	if checkSupported("unsupported") {
		t.FailNow()
	}
}

func TestSetDitherSettings_DoesNotReturnError(t *testing.T) {
	defer func() {
		if err := recover(); err != nil {
			t.FailNow()
		}
	}()

	ctx := swresample.SwrAlloc()
	nat, _ := native.NewContext()
	lav := LibAV{swrCtx: ctx, Native: nat}
	setupSwr_prepareLav(&lav)
	err := lav.Native.SetDitherSettings(DITHER_DEFAULT, SWR_ENGINE_SWR)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
}

func TestSetDitherSettings_IncorrectDither(t *testing.T) {
	defer avutil.AvLogSetLevel(avutil.AvLogGetLevel())
	avutil.AvLogSetLevel(avutil.AV_LOG_QUIET)
	ctx := swresample.SwrAlloc()
	nat, _ := native.NewContext()
	lav := LibAV{swrCtx: ctx, Native: nat}
	err := lav.Native.SetDitherSettings(-1, SWR_ENGINE_SWR)
	if err == nil {
		t.FailNow()
	}
}

func TestSetDitherSettings_IncorrectResampler(t *testing.T) {
	defer avutil.AvLogSetLevel(avutil.AvLogGetLevel())
	avutil.AvLogSetLevel(avutil.AV_LOG_QUIET)
	ctx := swresample.SwrAlloc()
	nat, _ := native.NewContext()
	lav := LibAV{swrCtx: ctx, Native: nat}
	err := lav.Native.SetDitherSettings(DITHER_DEFAULT, -1)
	if err == nil {
		t.FailNow()
	}
}

func TestSetupSwr_NoFrame(t *testing.T) {
	ctx := swresample.SwrAlloc()
	defer avutil.AvFree(unsafe.Pointer(ctx))
	nat, _ := native.NewContext()
	lav := LibAV{swrCtx: ctx, Native: nat}

	err := lav.setupSwr(nil)
	if err != os.ErrInvalid {
		t.FailNow()
	}
}

func TestSetupSwr_InvalidChannels(t *testing.T) {
	defer avutil.AvLogSetLevel(avutil.AvLogGetLevel())
	avutil.AvLogSetLevel(avutil.AV_LOG_QUIET)
	ctx := swresample.SwrAlloc()
	defer avutil.AvFree(unsafe.Pointer(ctx))
	frame := avutil.AvFrameAlloc()
	defer avutil.AvFrameFree(frame)
	nat, _ := native.NewContext()
	lav := LibAV{swrCtx: ctx, Native: nat}

	setupSwr_prepareLav(&lav)
	setupSwr_prepareFrame(lav, frame)

	lav.override_channels = -1

	err := lav.setupSwr(frame)
	if err == nil {
		t.FailNow()
	}
}

func TestSetupSwr_OK(t *testing.T) {
	defer avutil.AvLogSetLevel(avutil.AvLogGetLevel())
	avutil.AvLogSetLevel(avutil.AV_LOG_TRACE)
	ctx := swresample.SwrAlloc()
	defer avutil.AvFree(unsafe.Pointer(ctx))
	frame := avutil.AvFrameAlloc()
	defer avutil.AvFrameFree(frame)
	nat, _ := native.NewContext()
	lav := LibAV{swrCtx: ctx, Native: nat}

	setupSwr_prepareLav(&lav)
	setupSwr_prepareFrame(lav, frame)

	/// Assumption: LibAVExt.ChangeTargetFormat() has been called, so params are in valid ranges.

	err := lav.setupSwr(frame)
	if err != nil {
		t.FailNow()
	}
}
