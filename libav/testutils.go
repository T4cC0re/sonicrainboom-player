package libav

/*
	#cgo pkg-config: --static
	#include <libavutil/frame.h>
	#include <stdlib.h>
*/
import "C"
import (
	"gitlab.com/T4cC0re/goav/avutil"
	"unsafe"
)

func setupSwr_prepareLav(lav *LibAV) {
	lav.override_depth = 32
	lav.override_channels = 2
	lav.override_channel_layout = AV_CH_LAYOUT_STEREO
	lav.override_sample_fmt = AV_SAMPLE_FMT_S32
	lav.override_sample_rate = 44100
	//lav.Native.SetOverrides(
	//	true,
	//	lav.override_sample_fmt,
	//	lav.override_sample_rate,
	//	lav.override_depth,
	//	lav.override_channels,
	//	lav.override_channel_layout,
	//)
	//lav.Native.SetOnContext(nil, nil, nil, nil, unsafe.Pointer(lav.swrCtx), nil)
}

func setupSwr_prepareFrame(lav LibAV, frame *avutil.Frame) {
	c_frame := (*C.struct_AVFrame)(unsafe.Pointer(frame))
	(*c_frame).format = C.int32_t(lav.override_sample_fmt)
	(*c_frame).sample_rate = C.int32_t(lav.override_sample_rate)
	(*c_frame).channels = C.int32_t(lav.override_channels)
	(*c_frame).channel_layout = C.uint64_t(lav.override_channel_layout)
}
