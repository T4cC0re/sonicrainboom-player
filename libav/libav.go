package libav

import (
	"errors"
	"github.com/gordonklaus/portaudio"
	"gitlab.com/T4cC0re/goav/avcodec"
	"gitlab.com/T4cC0re/goav/avformat"
	"gitlab.com/T4cC0re/goav/avutil"
	"gitlab.com/T4cC0re/goav/swresample"
	. "gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	"gitlab.com/T4cC0re/sonicrainboom-player/native"
	timetrack "gitlab.com/T4cC0re/time-track"
	"os"
	"runtime"
	"runtime/debug"
	"sync"
	"time"

	"reflect"
	"unsafe"
)

/*
	#cgo pkg-config: --static libavutil-srb libavcodec-srb libswresample-srb
	#include <libavutil/frame.h>
	#include <libavutil/opt.h>
	#include <libavcodec/avcodec.h>
	#include <libswresample/swresample.h>
	#include <stdlib.h>
*/
import "C"

var initialized bool = false

type LibAV struct {
	bufferMayBeFilled       chan struct{}
	rate                    int
	channels                int
	sample_fmt              int
	channel_layout          uint
	resample                bool
	override_sample_fmt     int
	override_sample_rate    int
	override_depth          uint8
	override_channels       int
	override_channel_layout uint
	dither                  string
	forceStereo             bool
	depth                   uint8
	ctx                     *avformat.Context
	stream                  *avformat.Stream
	avCodecContext          *avcodec.Context
	codec                   *avcodec.Codec
	swrCtx                  *swresample.Context
	dict                    *C.struct_AVDictionary
	mutex                   *sync.Mutex
	buffers_sent            *uint
	filename                string
	resampler               string
	dsd                     uint16 //0, 64,128,256, etc.
	//mqa                     *MQAInformation
	mqa    native.MQA
	Native native.Native
}

type LibAVExt struct {
	lav *LibAV
}

/**
 * @deprecated: Use sampleBitsByFormat and the actual sample_fmt instead
 */
func sampleTypeByFormat(sample_fmt int, depth uint8) (sampleType interface{}) {
	defer timetrack.TimeTrack(time.Now())
	switch sample_fmt {
	case C.AV_SAMPLE_FMT_U8, C.AV_SAMPLE_FMT_U8P:
		sampleType = uint8(0)
	case C.AV_SAMPLE_FMT_DBL, C.AV_SAMPLE_FMT_DBLP:
		sampleType = float64(0)
	case C.AV_SAMPLE_FMT_FLT, C.AV_SAMPLE_FMT_FLTP:
		sampleType = float32(0)
	case C.AV_SAMPLE_FMT_S16, C.AV_SAMPLE_FMT_S16P:
		sampleType = int16(0)
	case C.AV_SAMPLE_FMT_S32, C.AV_SAMPLE_FMT_S32P:
		if depth == 24 {
			sampleType = portaudio.Int24{}
		} else {
			sampleType = int32(0)
		}
	}
	return
}

//func sampleBitsByFormat(sample_fmt int, depth uint8) (uint8) {
//	defer timetrack.TimeTrack(time.Now())
//	switch sample_fmt {
//	case C.AV_SAMPLE_FMT_U8, C.AV_SAMPLE_FMT_U8P:
//		return 8
//	case C.AV_SAMPLE_FMT_DBL, C.AV_SAMPLE_FMT_DBLP:
//		return 64
//	case C.AV_SAMPLE_FMT_FLT, C.AV_SAMPLE_FMT_FLTP:
//		return 32
//	case C.AV_SAMPLE_FMT_S16, C.AV_SAMPLE_FMT_S16P:
//		return 16
//	case C.AV_SAMPLE_FMT_S32, C.AV_SAMPLE_FMT_S32P:
//		if depth == 24 {
//		return 24
//		} else {
//		return 32
//		}
//	}
//	return 0
//}

func (ext LibAVExt) GetInfo() (string, int, int, interface{}, string, bool, uint16) {
	defer timetrack.TimeTrack(time.Now())
	fmt_string := av_get_sample_fmt_string(ext.lav.override_sample_fmt)
	if ext.lav.override_sample_fmt == C.AV_SAMPLE_FMT_S32 && ext.lav.override_depth == 24 {
		fmt_string = "s24"
	}
	if ext.lav.override_sample_fmt == C.AV_SAMPLE_FMT_S32P && ext.lav.override_depth == 24 {
		fmt_string = "s24p"
	}
	return ext.lav.filename, ext.lav.override_sample_rate, ext.lav.override_channels, sampleTypeByFormat(ext.lav.override_sample_fmt, ext.lav.override_depth), fmt_string, ext.lav.resample, ext.lav.dsd
}

//TODO: Migrate to C
//func (lav *LibAV) fixDepth() {
//	defer timetrack.TimeTrack(time.Now())
//
//	switch uint(reflect.ValueOf(*lav.avCodecContext).FieldByName("codec_id").Uint()) {
//	case uint(C.AV_CODEC_ID_PCM_S24LE),
//		uint(C.AV_CODEC_ID_PCM_S24BE),
//		uint(C.AV_CODEC_ID_PCM_S24LE_PLANAR):
//		// There is no AV_CODEC_ID_PCM_S24BE_PLANAR in FFMPEG
//		lav.depth = 24
//		lav.override_sample_fmt = C.AV_SAMPLE_FMT_S32 // We force non-planar audio everywhere anyways
//	default:
//		if lav.depth == 0 {
//			lav.depth = sampleBitsByFormat(lav.sample_fmt, 0)
//		}
//	}
//}

/**
ChangeTargetFormat: Change the target format to be resampled to (if required). If called after player.InitSong this will be ugly.
*/
func (ext LibAVExt) ChangeTargetFormat(
	target_fmt int,
	target_depth uint8,
	target_rate int,
	forceStereo bool,
) {
	defer timetrack.TimeTrack(time.Now())
	ext.lav.mutex.Lock()
	defer ext.lav.mutex.Unlock()

	log.Infof("", target_fmt, target_depth, target_rate, forceStereo)
	ext.lav.Native.ChangeTargetFormat(target_fmt, target_depth, target_rate, forceStereo)
	log.Infof("after ext.lav.Native.ChangeTargetFormat\n%+v\n", ext.lav.Native.Ctx)

	ext.lav.override_sample_rate = int(reflect.ValueOf(*(ext.lav.Native.Ctx)).FieldByName("override_sample_rate").Int())
	ext.lav.override_channels = int(reflect.ValueOf(*(ext.lav.Native.Ctx)).FieldByName("override_channels").Int())
	ext.lav.override_depth = uint8(reflect.ValueOf(*(ext.lav.Native.Ctx)).FieldByName("override_depth").Uint())
	ext.lav.override_sample_fmt = int(reflect.ValueOf(*(ext.lav.Native.Ctx)).FieldByName("override_sample_fmt").Int())
	ext.lav.override_channel_layout = uint(reflect.ValueOf(*(ext.lav.Native.Ctx)).FieldByName("override_channel_layout").Uint())
	ext.lav.resample = reflect.ValueOf(*(ext.lav.Native.Ctx)).FieldByName("resample").Bool()

	/** replaced GoCode

	var (
		resample           bool
		new_fmt            int
		new_depth          uint8
		new_rate           int
		new_channels       int
		new_channel_layout uint
	)
	log.Debugf(
		"rasample: %t, old_fmt: %s, old_depth: %d, old_rate: %d",
		false,
		av_get_sample_fmt_string(ext.lav.sample_fmt),
		ext.lav.depth,
		ext.lav.rate,
	)
	new_channels = ext.lav.channels
	new_channel_layout = ext.lav.channel_layout
	new_depth = ext.lav.depth
	new_fmt = ext.lav.sample_fmt
	new_rate = ext.lav.rate

	if av_sample_fmt_is_planar(new_fmt) {
		new_fmt = av_get_alt_sample_fmt(new_fmt, false)
	}
	if av_sample_fmt_is_planar(target_fmt) {
		target_fmt = av_get_alt_sample_fmt(target_fmt, false)
	}

	// This will override the target_fmt. If the input format or target_depth was floating point (and the depth allows) the target_fmt will be floating point again
	if target_depth > 0 {
		new_depth = target_depth
		switch new_depth {
		case 64:
			target_fmt = C.AV_SAMPLE_FMT_DBL
		case 32:
			switch true {
			case
				target_fmt == C.AV_SAMPLE_FMT_DBL,
				new_fmt == C.AV_SAMPLE_FMT_DBL,
				target_fmt == C.AV_SAMPLE_FMT_FLT,
				new_fmt == C.AV_SAMPLE_FMT_FLT:
				// If the input was supposed to be floating point, make this flt32
				target_fmt = C.AV_SAMPLE_FMT_FLT
			default:
				// Otherwise s32, please
				target_fmt = C.AV_SAMPLE_FMT_S32
			}
		case 24:
			// Despite only 24-bits being used, libav will handle it in 32-bit with the upper 8 bits == 0x00
			target_fmt = C.AV_SAMPLE_FMT_S32
		default: // case 16:
			target_fmt = C.AV_SAMPLE_FMT_S16
		case 8:
			target_fmt = C.AV_SAMPLE_FMT_U8
		}
	}

	if target_rate > 0 {
		new_rate = target_rate
	} else {
		new_rate = ext.lav.rate
	}
	if forceStereo && ext.lav.channels != 2 {
		new_channels = 2
		new_channel_layout = C.AV_CH_LAYOUT_STEREO_DOWNMIX
	}
	if target_fmt != C.AV_SAMPLE_FMT_NONE {
		// Do not convert formats, but do change the fmt to non-planar in every case.
		new_fmt = av_get_alt_sample_fmt(target_fmt, false)
	}

	new_depth = sampleBitsByFormat(new_fmt, new_depth)

	// Do we need to resample?
	if new_channel_layout != ext.lav.channel_layout ||
		new_channels != ext.lav.channels ||
		new_depth != ext.lav.depth ||
		new_fmt != ext.lav.sample_fmt ||
		new_rate != ext.lav.rate {
		resample = true
	}

	ext.lav.resample = resample
	ext.lav.override_sample_rate = new_rate
	ext.lav.override_sample_fmt = new_fmt
	ext.lav.override_depth = new_depth
	ext.lav.override_channels = new_channels
	ext.lav.override_channel_layout = new_channel_layout

	ext.lav.Native.SetOverrides(
		ext.lav.resample,
		ext.lav.override_sample_fmt,
		ext.lav.override_sample_rate,
		ext.lav.override_depth,
		ext.lav.override_channels,
		ext.lav.override_channel_layout,
	)
	*/

	// TODO: Wording around 'bit-perfect' resampling when only touching the sample_fmt and nothing else.
	if ext.lav.resample {
		log.Infof(
			"Will convert to %d Hz %d Channels, %d bit/sample, sample_fmt: %s",
			ext.lav.override_sample_rate,
			ext.lav.override_channels,
			ext.lav.override_depth,
			av_get_sample_fmt_string(ext.lav.override_sample_fmt),
		)
	} else {
		log.Infof("No resample required. Format: %d Hz %d Channels, %d bit/sample, sample_fmt: %s",
			ext.lav.override_sample_rate,
			ext.lav.override_channels,
			ext.lav.override_depth,
			av_get_sample_fmt_string(ext.lav.override_sample_fmt),
		)
	}
}

func Init() {
	defer timetrack.TimeTrack(time.Now())

	// Register formats and codecs
	log.Debug("Registering libavformat formats...")
	avformat.AvRegisterAll()
	log.Debug("Registering libavcodec codecs...")
	avcodec.AvcodecRegisterAll()

	// Make sure we can use the net
	log.Debug("Initializing network...")
	avformat.AvformatNetworkInit()
}

func NewLibAV(filename string, forceRate int, forceDepth uint8, forceStereo bool, experimental bool, dither string, resampler string, token string) (dec *Decoder) {
	if !initialized {
		Init()
		initialized = true
	}
	defer timetrack.TimeTrack(time.Now())

	/// WARNING: This is actively being ported to C.
	/// Any commented out code has been replaced with C. There are reflection hacks to bring back state from C to Go.

	nativeCtx, err := native.NewContext(filename, forceRate, forceDepth, forceStereo, dither, resampler, token)
	if err != nil {
		panic(err)
	}
	lav := LibAV{
		Native: nativeCtx,
	}
	//
	//lav.dict = nil
	//if experimental {
	//	setting := C.CString("strict")
	//	value := C.CString("experimental")
	//	C.av_dict_set(&lav.dict, setting, value, 0)
	//	C.free(unsafe.Pointer(setting))
	//	C.free(unsafe.Pointer(value))
	//}
	//
	//{
	//	setting := C.CString("user_agent")
	//	value := C.CString("sonicrainboom-player")
	//	C.av_dict_set(&lav.dict, setting, value, 0)
	//	C.free(unsafe.Pointer(setting))
	//	C.free(unsafe.Pointer(value))
	//}
	//
	//if token != "" {
	//	setting := C.CString("headers")
	//	value := C.CString(fmt.Sprintf("Authorization: Bearer %s\r\n", token))
	//	C.av_dict_set(&lav.dict, setting, value, 0)
	//	C.free(unsafe.Pointer(setting))
	//	C.free(unsafe.Pointer(value))
	//}
	//
	//lav.Native.SetOnContext(nil, nil, nil, nil, nil, unsafe.Pointer(lav.dict))
	//
	//log.Debug("Initializing libav decoder...")
	//// Get a context
	//log.Debug("Allocating context...")
	//lav.ctx = avformat.AvformatAllocContext()
	//lav.Native.SetOnContext(unsafe.Pointer(lav.ctx), nil, nil, nil, nil, nil)
	//log.Debug("Initialized libav decoder successfully")
	//
	//
	//log.Infof("Loading file '%s'...", filename)
	//if avformat.AvformatOpenInput(&lav.ctx, filename, nil, (**avutil.Dictionary)(unsafe.Pointer(&lav.dict))) != 0 {
	//	log.Fatalf("Unable to open file %s", filename)
	//}
	//
	//log.Debug("Loading additional stream headers...")
	//if lav.ctx.AvformatFindStreamInfo(nil) < 0 {
	//	log.Fatalf("Unable to find additional stream headers")
	//}
	//
	//log.Infof("format dump:")
	//lav.ctx.AvDumpFormat(0, "sonicrainboom-player input", 0)
	//
	//log.Debug("Loading audio stream...")
	//for _, istream := range lav.ctx.Streams() {
	//	if istream.Codec().GetCodecType() == C.AVMEDIA_TYPE_AUDIO {
	//		lav.stream = istream
	//		break
	//	}
	//}
	//if lav.stream == nil {
	//	log.Fatalf("No audio stream found")
	//}
	//lav.Native.SetOnContext(nil, unsafe.Pointer(lav.stream), nil, nil, nil, nil)
	//
	//log.Debug("Loading Codec...")
	//codecContext := lav.stream.Codec()
	//if codecContext == nil {
	//	log.Fatalf("Unable to load avCodecContext")
	//	return // To make go linter happy, fatal above will quit
	//} else {
	//	log.Tracef("%+v", *codecContext)
	//}
	//
	//log.Debug("Loading decoder...")
	//lav.codec = avcodec.AvcodecFindDecoder(avcodec.CodecId(codecContext.GetCodecId()))
	//if lav.codec == nil {
	//	log.Fatalf("Unable to load decoder")
	//}
	//lav.Native.SetOnContext(nil, nil, nil, unsafe.Pointer(lav.codec), nil, nil)
	//
	//lav.avCodecContext = (*avcodec.Context)(unsafe.Pointer(codecContext))
	//if lav.avCodecContext == nil {
	//	log.Fatalf("Unable to allocate codec context")
	//}
	//lav.Native.SetOnContext(nil, nil, unsafe.Pointer(lav.avCodecContext), nil, nil, nil)
	//
	//log.Infof("%+v", lav.Native.Ctx)

	// Retrieve fields from native context
	lav.rate = int(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("sample_rate").Int())
	lav.channels = int(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("channels").Int())
	lav.depth = uint8(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("depth").Uint())
	lav.sample_fmt = int(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("sample_fmt").Int())
	lav.channel_layout = uint(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("channel_layout").Uint())
	lav.dsd = uint16(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("dsd").Uint())
	//var dsdSuffix string
	//if lav.dsd != 0 {
	//	log.Infof("Detected DSD%d", lav.dsd)
	//	dsdSuffix = fmt.Sprintf(" DSD%d", lav.dsd)
	//}
	//
	//log.Infof("Format determined as %d Hz %d Channels, %d bit/sample, sample_fmt: %s%s",
	//	lav.rate,
	//	lav.channels,
	//	lav.depth,
	//	av_get_sample_fmt_string(lav.sample_fmt),
	//	dsdSuffix,
	//)
	//
	//if lav.avCodecContext.AvcodecOpen2(lav.codec, (**avcodec.Dictionary)(unsafe.Pointer(&lav.dict))) < 0 {
	//	log.Fatalf("Failed to open decoder for stream %d", lav.stream.Index())
	//}
	//
	//log.Tracef("%+v", *lav.stream)
	////return
	//
	//log.Infof("Loaded file '%s' successfully", filename)

	// Bring over all the C structs we initialized to Go
	lav.dict = (*C.struct_AVDictionary)(unsafe.Pointer(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("avDictionary").Pointer()))
	lav.ctx = (*avformat.Context)(unsafe.Pointer(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("avFormatContext").Pointer()))
	lav.stream = (*avformat.Stream)(unsafe.Pointer(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("avStream").Pointer()))
	lav.avCodecContext = (*avcodec.Context)(unsafe.Pointer(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("avCodecContext").Pointer()))
	lav.codec = (*avcodec.Codec)(unsafe.Pointer(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("avCodec").Pointer()))
	lav.swrCtx = (*swresample.Context)(unsafe.Pointer(reflect.ValueOf(*(lav.Native.Ctx)).FieldByName("swrContext").Pointer()))

	lav.bufferMayBeFilled = make(chan struct{}, 0)
	lav.buffers_sent = new(uint)
	lav.filename = filename
	lav.mutex = new(sync.Mutex)
	lav.mqa, err = native.NewMQAContext()
	if err != nil {
		return
	}

	// Even though we might not resample, let's just allocate this here.
	lav.swrCtx = swresample.SwrAlloc()
	//lav.Native.SetOnContext(nil, nil, nil, nil, unsafe.Pointer(lav.swrCtx), nil)

	extLav := LibAVExt{
		&lav,
	}

	extLav.ChangeTargetFormat(C.AV_SAMPLE_FMT_NONE, forceDepth, forceRate, forceStereo)

	// For MQA detection we must not dither. But resampling might be ok, if we just change the sample_fmt
	{
		lav.dither = "none"
		lav.resampler = "swr"
		lav.DetectMQA()
		if lav.mqa.IsMQA() {
			if lav.resample {
				log.Infof("bit-perfect resampling (sample format conversion) to preserve MQA")
			}
		} else {
			// When it turns out the input is not MQA, we can resample with user settings while playback
			lav.dither = dither
			lav.resampler = resampler
		}
	}

	log.Infof("Native ctx: %+v", lav.Native)

	dec1 := Decoder(extLav)
	dec = &dec1
	return
}

func (ext LibAVExt) SentBuffers() uint {
	defer timetrack.TimeTrack(time.Now())
	return *ext.lav.buffers_sent
}

func (ext LibAVExt) GetMQA() MQAInformation {
	defer timetrack.TimeTrack(time.Now())
	//return ext.lav.mqa
	return ext.lav.mqa.GetLegacyObject()
}

func (ext LibAVExt) TryStop() {
	defer timetrack.TimeTrack(time.Now())
	close((*ext.lav).bufferMayBeFilled)
}

func (ext LibAVExt) FillBuffer(targetBuffersize uint, buffercount uint) (bufferchain chan *AudioBuffer) {
	defer timetrack.TimeTrack(time.Now())
	ext.lav.mutex.Lock()
	defer func() {
		if err := recover(); err != nil {
			if err.(error).Error() == "send on closed channel" {
				return
			}
			panic(err)
		}
	}()

	bufferchain = make(chan *AudioBuffer, buffercount)

	if targetBuffersize == 0 {
		log.Fatalf("targetBuffersize is 0")
	}

	log.Debugf("Filling %d buffers with up to %d bytes each", buffercount, targetBuffersize)

	frame := avutil.AvFrameAlloc()
	AssertNotNil(frame, "Error allocating frame")
	packet := avcodec.AvPacketAlloc()
	AssertNotNil(packet, "Error allocating packet")

	log.Tracef("lav.override_sample_fmt: %d", ext.lav.override_sample_fmt)

	go ext.lav.AVReadFrameLoop(bufferchain, targetBuffersize, frame, packet)

	return bufferchain
}

func (lav *LibAV) DetectMQA() {
	defer timetrack.TimeTrack(time.Now())
	lav.mutex.Lock()
	defer lav.mutex.Unlock()

	switch true {
	case lav.override_channels != 2,
		lav.override_sample_rate != 44100 && lav.override_sample_rate != 48000,
		lav.override_depth != 16 && lav.override_depth != 24,
		lav.dsd > 0:
		log.Infof("Not a qualifying MQA stream (16/24bit, 44100/48000kHz, stereo)")
		lav.mqa.SetFinished() // Otherwise the MQA analysis will be 'pending'
		return
	default:
		// All good. You may pass.
	}

	// Alloc stuff
	var gotFrame int
	var err error
	var tmpaudio []byte
	var nb_samples int
	var frameCounter uint64
	var framesWithoutMQA uint8 = 0
	//var mqaAnalysisBuffer uint64 = 0
	frame := avutil.AvFrameAlloc()
	AssertNotNil(frame, "Error allocating frame")
	defer avutil.AvFrameFree(frame)
	packet := avcodec.AvPacketAlloc()
	AssertNotNil(packet, "Error allocating packet")
	avutil.AvFreep(unsafe.Pointer(packet))

	// Decode packets
packet:
	for lav.ctx.AvReadFrame(packet) >= 0 {
		if packet.StreamIndex() != lav.stream.Index() {
			// ignore frames not from the audio stream
			continue packet
		}
		response := lav.avCodecContext.AvcodecDecodeAudio4((*avcodec.Frame)(unsafe.Pointer(frame)), &gotFrame, packet)
		if response == avutil.AvErrorEAGAIN || response == avutil.AvErrorEOF {
			log.Debug(avutil.ErrorFromCode(response))
			continue packet
		} else if response < 0 {
			log.Fatal(avutil.ErrorFromCode(response))
		}
		if gotFrame <= 0 {
			continue packet
		}
		nb_samples, tmpaudio, err = samplesFromFrame(*lav, frame)
		AssertNotErr(err)

		log.Debugf("Looking for MQA in samples of frame %d", frameCounter)
		//if lav.mqa.AnalyzeSamples(&mqaAnalysisBuffer, uint(nb_samples), &tmpaudio, lav.override_depth, lav.override_channels) {
		if lav.mqa.AnalyzeSamples(uint(nb_samples), &tmpaudio, lav.override_depth, lav.override_channels) {
			log.Debugf("MQA found in samples after %d frames", framesWithoutMQA)
			framesWithoutMQA = 0
			break
		} else {
			log.Debugf("No MQA samples found")
			framesWithoutMQA++
			if framesWithoutMQA == 0xf {
				log.Debugf("Giving up MQA search")
				// Pending state is usually cleared in AnalyzeSamples(), but only in the case of MQA being detected.
				//lav.mqa.Pending = false
				lav.mqa.SetFinished()
				break
			}
		}
		frameCounter++
	}

	lav.ctx.AvSeekFrame(lav.stream.Index(), 0, 1) // 1 = AVSEEK_FLAG_BACKWARD
}

/**
 * Must be called holding the lav.mutex lock by the caller. Will be unlocked upon return
 */
func (lav LibAV) AVReadFrameLoop(bufferchain chan *AudioBuffer, targetBuffersize uint, frame *avutil.Frame, packet *avcodec.Packet) {
	defer func() {
		avutil.AvFrameFree(frame)
		avutil.AvFreep(unsafe.Pointer(packet))

		lav.mutex.Unlock()
		close(bufferchain)
	}()

	written := 0
	var sample int64
	var gotFrame int

	buf := NewAudioBuffer(targetBuffersize)
	audio := &buf
	audio.Lock()

	log.Tracef("AvReadFrame")
packet:
	for lav.ctx.AvReadFrame(packet) >= 0 {
		// Relock mutex to give the chance of using this to another thread (e.g. shutdown)
		lav.mutex.Unlock()
		lav.mutex.Lock()

		select {
		case <-lav.bufferMayBeFilled:
			log.Info("Stopping filling")
			return
		default:

		}

		lav.Native.BufferFromPacket(unsafe.Pointer(packet))

		log.Tracef("Read packet")
		if packet.StreamIndex() != lav.stream.Index() {
			// ignore frames not from the audio stream
			continue packet
		}
		log.Tracef("Decoding")
		response := lav.avCodecContext.AvcodecDecodeAudio4((*avcodec.Frame)(unsafe.Pointer(frame)), &gotFrame, packet)
		if response == avutil.AvErrorEAGAIN || response == avutil.AvErrorEOF {
			log.Debug(avutil.ErrorFromCode(response))
			continue packet
		} else if response < 0 {
			log.Fatal(avutil.ErrorFromCode(response))
		}
		log.Tracef("after AvcodecDecodeAudio4")
		if gotFrame <= 0 {
			//response = lav.avCodecContext.AvcodecDecodeAudio4(frame, &gotFrame, packet)
			continue packet
		}

		log.Tracef("Start of frame")
		log.Tracef("Frame: %+v", frame)

		log.Tracef("lav.resample")

		var err error
		var tmpaudio []byte
		var nb_samples int

		log.Tracef("before samplesFromFrame")
		nb_samples, tmpaudio, err = samplesFromFrame(lav, frame)
		log.Tracef("got samples From Frame")
		if err != nil {
			panic(err)
		}
		log.Tracef("End of frame")
		sample += int64(nb_samples)

		bufferOffset := 0
		maxCallsWithoutData := 1
	buffer:
		for {
			maxCallsWithoutData--
			if maxCallsWithoutData < 0 {
				tmpaudio = nil
				break buffer
			}
			log.Debugf("before written %d, cap(*audio) %d len(tmpaudio) %d", written, cap(*audio.Data), len(tmpaudio))
			writtenInThisCall := native.FillBuffer(tmpaudio, bufferOffset, audio.Data, written)

			bufferOffset += writtenInThisCall

			written += writtenInThisCall

			log.Debugf("writtenInThisCall %d, bufferOffset %d, written %d, cap(*audio) %d len(tmpaudio) %d", writtenInThisCall, bufferOffset, written, cap(*audio.Data), len(tmpaudio))

			if writtenInThisCall > 0 {
				maxCallsWithoutData++
			}

			if written == cap(*audio.Data) {
				// Buffer complete
				log.Debugf("Buffer complete (%d bytes), sending", written)
				log.Debugf("Samples in buffer: %d", uint(written)/uint(lav.override_depth/8))

				audio.Unlock()
			trySend:
				//for {
				select {
				case <-lav.bufferMayBeFilled:
					log.Info("Giving up on sending")
					tmpaudio = nil
					return
				case bufferchain <- audio:
					*lav.buffers_sent++
					break trySend
				}
				if *lav.buffers_sent%8 == 0 {
					runtime.GC()
					debug.FreeOSMemory()
				}
				buf2 := NewAudioBuffer(targetBuffersize)
				audio = &buf2
				audio.Lock()
				written = 0
			} else {
				log.Debugf("buffer incomplete  (%d bytes),", written)
			}
			if bufferOffset >= len(tmpaudio) {
				// All data was written to buffers
				log.Debugf("All data was written to buffers")
				tmpaudio = nil
				break buffer
			}
		}
	}

	log.Infof("All blocks read. Samples: %d, seconds: %f", sample*4096, float64(sample)/float64(lav.rate))

	select {
	case <-lav.bufferMayBeFilled:
		log.Info("Ignoring remaining data")
		return
	default:

	}
	if written != cap(*audio.Data) {
		// Buffer complete
		log.Debugf("Final buffer complete (%d bytes), sending", written)

		audio.Unlock()
		bufferchain <- audio
		*lav.buffers_sent++
	}
}

/**
setSliceData: Manipulates the passed slice to use the passed data pointer
*/
func setSliceData(slice *[]byte, size int, data uintptr) {
	defer timetrack.TimeTrack(time.Now())
	log.Debugf("setting slice 0x%x to %d bytes @ 0x%x", unsafe.Pointer(slice), size, data)
	if data == 0 {
		panic(os.ErrInvalid)
	}
	sh := (*reflect.SliceHeader)(unsafe.Pointer(slice))
	// This becomes invisible to Go, so we'll free it ourselves.
	C.free(unsafe.Pointer(sh.Data))
	sh.Data = data
	sh.Len = size
	sh.Cap = sh.Len
}

func samplesFromFrame(lav LibAV, frame *avutil.Frame) (int, []byte, error) {
	defer timetrack.TimeTrack(time.Now())
	var data_size int
	var samples int
	var raw_data []byte
	var err error
	sampleSize := lav.override_depth / 8

	if lav.resample {
		data_size, samples, raw_data, err = newResample(lav, frame)
		if err != nil {
			return 0, nil, err
		}
	} else {
		cFrame := (*C.struct_AVFrame)(unsafe.Pointer(frame))
		samples = int((*cFrame).nb_samples)

		data_size = int(C.av_samples_get_buffer_size(nil, (*cFrame).channels,
			(*cFrame).nb_samples, int32((*cFrame).format), 1))

		setSliceData(&raw_data, data_size, uintptr(unsafe.Pointer((*cFrame).data[0])))
	}

	// Adjust for 24-bit PCM
	if sampleSize == 3 {
		data_size = data_size / 4 * 3
	}

	internalBuffer := make([]byte, data_size)
	log.Tracef("cap(internalBuffer): %d", cap(internalBuffer))

	log.Tracef("writing to buffer")
	written := native.CopySample(raw_data, &internalBuffer, sampleSize)

	if written < 0 {
		return 0, nil, errors.New("could not copy samples")
	}

	return samples, internalBuffer, nil
}

func newResample(lav LibAV, frame *avutil.Frame) (data_size, samples int, raw_data []byte, err error) {
	defer timetrack.TimeTrack(time.Now())
	cFrame := (*C.struct_AVFrame)(unsafe.Pointer(frame))

	ratio := float64(lav.override_sample_rate) / float64(lav.rate)

	log.Debugf("target sample ratio: %f", ratio)
	log.Debugf("source samples: %d", int32((*cFrame).nb_samples))
	log.Debugf("target samples: %d", int32(float64((*cFrame).nb_samples)*ratio))

	if lav.swrCtx.SwrIsInitialized() == 0 {
		err = lav.setupSwr(frame)
		if err != nil {
			return 0, 0, nil, err
		}
		if response := lav.swrCtx.SwrInit(); response < 0 {
			log.Debugf("lav.swrCtx.SwrInit Fail")
			return 0, 0, nil, avutil.ErrorFromCode(response)
		}
	}
	var buf *C.uchar = nil

	data_size = int(C.av_samples_alloc(&buf, nil, C.int32_t(lav.override_channels), C.int32_t(float64((*cFrame).nb_samples)*ratio), int32(lav.override_sample_fmt), 1))
	if data_size < 0 {
		return 0, 0, nil, avutil.ErrorFromCode(data_size)
	} else {
		log.Debugf("newResample alloc OK: %d", data_size)
	}

	setSliceData(&raw_data, data_size, uintptr(unsafe.Pointer(buf)))
	log.Debugf("len raw_data: %d", len(raw_data))

	samples = int(C.swr_convert(
		(*C.struct_SwrContext)(unsafe.Pointer(lav.swrCtx)),
		(**C.uchar)(unsafe.Pointer(&buf)),
		C.int32_t(float64((*cFrame).nb_samples)*ratio),
		(**C.uchar)(unsafe.Pointer(&(*cFrame).data)),
		(*cFrame).nb_samples,
	))

	//log.Debugf("%+v", raw_data)

	log.Debugf("raw_data[size-1]: 0x%x, frames: %d", raw_data[(data_size/2)], samples)
	//return 0, 0, nil, os.ErrInvalid

	return
}

func (ext LibAVExt) Close() {
	defer timetrack.TimeTrack(time.Now())
	ext.lav.mutex.Lock()
	defer ext.lav.mutex.Unlock()
	defer timetrack.TimeTrack(time.Now())
	C.free(unsafe.Pointer(ext.lav.ctx))
	C.free(unsafe.Pointer(ext.lav.avCodecContext))
	C.free(unsafe.Pointer(ext.lav.stream))
	C.free(unsafe.Pointer(ext.lav.swrCtx))
	C.free(unsafe.Pointer(ext.lav.dict))
}

func DeInit() {
	defer timetrack.TimeTrack(time.Now())
	avformat.AvformatNetworkDeinit()
}
