package main

import (
	"errors"
	"flag"
	"fmt"
	"github.com/gordonklaus/portaudio"
	. "gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/libav"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	"gitlab.com/T4cC0re/sonicrainboom-player/native"
	"gitlab.com/T4cC0re/sonicrainboom-player/player"
	"os"
	"os/signal"
	"regexp"
	"runtime/pprof"
	"strings"
	"time"
)

var filename string
var srbId string
var dummy = false
var forceRate int
var experimental bool
var forceStereo bool
var buffercount uint = 4
var buffersizemultiplier uint = 1
var depth uint
var dither string
var resampler string
var dump string
var ver bool
var cpuprofile string
var deviceName string
var dacSwitchTime uint

var versionString = native.SrbVersion()

func init() {
	flag.StringVar(&filename, "file", "", "Filename to play")
	flag.StringVar(&srbId, "srb", "", "Connect to SonicRainBoom as a player")
	flag.StringVar(&dither, "dither_method", "high_shibata", "Dither method to use (see dither_method help for FFmpeg resampler)")
	flag.StringVar(&resampler, "resampler", "swr", "Resampler to use (swr or soxr). If soxr is used, 24-bit conversions will be clipeed. Use 16- or32-bit instead.")
	flag.BoolVar(&experimental, "experimental", experimental, "Enable experimental features on libav")
	flag.BoolVar(&forceStereo, "stereo", forceStereo, "Force stereo downmix")
	flag.IntVar(&forceRate, "rate", forceRate, "Force sample-rate on device and stream")
	flag.UintVar(&depth, "depth", depth, "Force bit-depth on device and stream")
	flag.UintVar(&buffercount, "buffercount", buffercount, "Keep this many buffers on hand")
	flag.UintVar(&buffersizemultiplier, "buffersizemultiplier", buffersizemultiplier, "Multiply calculated buffer-size by this")
	flag.BoolVar(&dummy, "dummy", dummy, "Do not play, just generate the samples and discard them (useful for memory debugging)")
	flag.StringVar(&dump, "dump", dump, "Dump raw audio buffer into this file")
	flag.BoolVar(&ver, "version", false, "Show Version")
	flag.StringVar(&cpuprofile, "cpuprofile", "", "write cpu profile to file")
	flag.UintVar(&dacSwitchTime, "dacswitchtime", 500, "time to wait (in ms) after switching formats on the DAC before playing")

	// FOR C HANDOVER ONLY
	flag.StringVar(&deviceName, "device", "", "Device name to play on (does nothing if using PulseAudio on Linux)")

}

func main() {
	flag.Parse()
	if ver {
		// versions are printed by the native_bridge
		os.Exit(0)
	}

	if cpuprofile != "" {
		f, err := os.Create(cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	defer func() {
		if err := recover(); err != nil {
			log.Error(err)
			panic(err)
		}
	}()

	pl := player.Player{
		DACSwitchTime: time.Duration(dacSwitchTime) * time.Millisecond,
	}
	//pl.Init()
	//if pl.ListDevices() {
	//	os.Exit(0)
	//}

	if srbId != "" {
		id := parseSRBID(srbId)
		if id.IsPlayerConnect() {
			//TODO: Connect to Websocket
			errCode := native.Connect(id.String())
			panic(errors.New(fmt.Sprintf("err %d, cannot connect to websocket at %s", errCode, id.String())))
		} else {
			panic(errors.New(fmt.Sprintf("srb parameter needs to be a connect URL. is '%s'", id.String())))
		}
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, os.Kill)

	go func() {
		for {
			pl.PrintStats()
			time.Sleep(time.Second * 10)
		}
	}()

	playback_stop := make(chan struct{}, 1)
	exit := make(chan struct{}, 1)
	queue := make(chan SRBPath)

	device := player.Device{DeviceInfo: nil}
	//device := player.Device{DeviceInfo: pl.GetDevice()}
	//device.DetectCapabilities()
	//log.Infof("Chosen device: %s, %+v", device.Name, device.Capabilities)

	files := strings.Split(filename, ";")

	log.Infof("Press Ctrl-C to stop.")
	go func() {
		for {
			select {
			case <-stop:
				log.Infof("Caught interrupt")
				playback_stop <- struct{}{}
				exit <- struct{}{}
				// If the above channels are consumed we will exit gracefully.
				// But exit hard after giving them 2 seconds to do so.
				time.Sleep(time.Second * 2)
				os.Exit(1)
			}
		}
	}()

	go func() {
		for _, filename := range files {
			filename = strings.TrimSpace(filename)
			if filename != "" {
				id := parseSRBID(filename)
				if id.IsPlayerConnect() {
					log.Errorf("Cannot connect to SRB as part of a file parameter")
				}

				queue <- id
			}
		}
	}()

	// C Handover! :D
	native.Handoff(
		files[0],
		forceRate,
		uint8(depth & 0xFF),
		forceStereo,
		dither,
		resampler,
		deviceName,
		pl.AuthToken,
	)

	os.Exit(0)

	for {
		select {
		case item := <-queue:
			play(item, forceRate, uint8(depth), forceStereo, experimental, dither, resampler, pl, device, playback_stop)
		case <-exit:
			log.Infof("quitting...")
			pl.DeInit()
			libav.DeInit()
			native.SRBShutdown()
			os.Exit(0)
		}
	}
}

func play(srbPath SRBPath, forceRate int, depth uint8, forceStereo bool, experimental bool, dither string, resampler string, pl player.Player, device player.Device, playback_stop chan struct{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error(err)
		}
	}()

	var decoder *Decoder
	var err error
	pl.AuthToken = srbPath.Auth
	decoder = libav.NewLibAV(srbPath.String(), forceRate, depth, forceStereo, experimental, dither, resampler, pl.AuthToken)
	AssertNotNil(decoder, "decoder did not initialize")
	log.Infof("decoder OK")
	log.Infof("MQA?: %v", (*decoder).GetMQA())

	// Test compat check
	//(*decoder).(libav.LibAVExt).ChangeTargetFormat(libav.AV_SAMPLE_FMT_S32, 32, 192000, forceStereo)

	if device.HostApi.Type == portaudio.CoreAudio {
		// macOS requires 32bit float apparently to play fine
		log.Warn("Running with CoreAudio backend. Need to resample to f32")
		(*decoder).(libav.LibAVExt).ChangeTargetFormat(libav.AV_SAMPLE_FMT_FLT, 32, 192000, forceStereo)
	}

	if !pl.CheckCapability(decoder, device) {
		// Emulated
		log.Warn("Selected format not supported. Switching to device capabilities...")
		// TODO: Detect actual device caps and set accordingly
		(*decoder).(libav.LibAVExt).ChangeTargetFormat(libav.AV_SAMPLE_FMT_FLT, 32, 352800, true)
	}

	err = pl.InitSong(&device, decoder, buffercount, buffersizemultiplier, dump)
	AssertNotErr(err)

	// Memory debugging
	if dummy {
		time.Sleep(time.Second)
		pl.Dummy()
		pl.Close()
		pl.PrintStats()
	} else {
		err = pl.Play(playback_stop)
		AssertNotErr(err)
		pl.Close()
	}
}

type SRBPath struct {
	Protocol   string
	Host       string
	ApiBase    string
	ObjectType string
	ObjectID   string
	Auth       string
	NonSRBURL  string
}

func (a SRBPath) String() string {
	if !a.IsSRBPath() {
		return a.NonSRBURL
	}
	if a.ObjectType == "song" {
		return fmt.Sprintf("%s://%s%s/songs/%s/play/raw", a.Protocol, a.Host, a.ApiBase, a.ObjectID)
	}
	if a.ObjectType == "connect" {
		return fmt.Sprintf("%s://%s%s/messagehub?playerauth=%s", a.Protocol, a.Host, a.ApiBase, a.Auth)
	}
	return fmt.Sprintf("%s://%s%s/%s/%s?playerauth=%s", a.Protocol, a.Host, a.ApiBase, a.ObjectType, a.ObjectID, a.Auth)
}

func (a SRBPath) AuthToken() string {
	return a.Auth
}

func (a SRBPath) IsPlayerConnect() bool {
	return a.ObjectType == "connect"
}

func (a SRBPath) IsSRBPath() bool {
	return a.NonSRBURL == ""
}

func parseSRBID(path string) (ret SRBPath) {
	var re = regexp.MustCompile(`(?mi)(?P<proto>srbu?)://(?P<host>[^/]+)/(?P<type>[^#/]+)(?:/(?P<id>\d+))?(?:#(?P<auth>.*))?`)
	match := re.FindStringSubmatch(path)
	if match == nil {
		ret.NonSRBURL = path
		return
	}
	results := map[string]string{}
	for i, name := range match {
		results[re.SubexpNames()[i]] = name
	}

	if results["proto"] == "srb" {
		ret.Protocol = "https"
	} else {
		ret.Protocol = "http"
	}
	ret.Host = results["host"]
	if !strings.Contains(ret.Host, ".") {
		ret.Host = fmt.Sprintf("%s.instance.sonicrainboom.rocks", ret.Host)
	}
	ret.ApiBase = "/api/v1"
	ret.ObjectType = results["type"]
	ret.ObjectID = results["id"]
	ret.Auth = results["auth"]

	return
}
