#!/usr/bin/env sh
echo "$@" > ./.ffmpeg_config.now
if ! [ -d ./ffmpeg/lib ]; then
  exit 1
fi
cmp ./.ffmpeg_config.now ./ffmpeg/lib/config.built 2>&1 >/dev/null
