include Makefile.variables

ifndef VARIABLES_DEFINED
  $(error "Could not load Makefile.variables!")
endif

all: dependencies/compiled bin/sonicrainboom-player$(SUFFIX) bin/sonicrainboom-player_debug$(SUFFIX) bin/srbutil$(SUFFIX)

include Makefile.buildx

bin $(TEMP_DIR) $(TEMP_DIR)/fuzz:
	mkdir -p $@

include Makefile.ffmpeg
include Makefile.native
include Makefile.player

clean_tmp:
	$(GO) clean -cache -testcache -modcache
	$(RM) -r '$(TEMP_DIR)'

clean: native/clean packaging/clean player/clean clean_tmp
	$(RM) -r $(SOURCE_DIR)/bin
	$(MAKE) -w -C dependencies clean

install_pkg_deps:
  ifdef PACMAN
	$(PACMAN) -Sy --noconfirm --needed alsa-lib base-devel ca-certificates curl git go nasm
  else
    ifdef BREW
	$(BREW) install autoconf libtool automake pkg-config golang nasm
    else
      ifdef APT
	DEBIAN_FRONTEND=noninteractive $(APT) update
	@echo "Installing build requirements..."
	DEBIAN_FRONTEND=noninteractive $(APT) install -y build-essential autoconf libtool nasm libasound-dev pkg-config curl
      else
	$(error "No supported package manager (apt/pacman/brew) found")
      endif
    endif
  endif

.PHONY: compile_commands.json
compile_commands.json:
	$(MAKE) -d all 2>/dev/null | bash -c 'tee <(compiledb --full-path -f -o compile_commands.json)'

ifneq ($(MACOS_ROSETTA),no)
  # On rosetta compatible systems, we need to compile both x86_64 and arm64
dependencies/compiled:
	# This is a convoluted way of getting rid of the environment to have make regenerate it via Makefile.variables.
	# Otherwise the current arch will bleed into the other arch, corrupting the build.
	env bash -c 'arch -arch x86_64 make -w -C "$(SOURCE_DIR)" dependencies/compiled_x86_64'
	env bash -c 'arch -arch arm64 make -w -C "$(SOURCE_DIR)" dependencies/compiled_arm64'
	touch $@
dependencies/compiled_arm64: | $(TEMP_DIR)
	$(MAKE) -w -C dependencies
	touch $@
dependencies/compiled_x86_64: | $(TEMP_DIR)
	$(MAKE) -w -C dependencies
	touch $@
else
dependencies/compiled: dependencies/compiled_$(ARCH)
	touch $@
dependencies/compiled_$(ARCH): | $(TEMP_DIR)
	$(MAKE) -w -C dependencies
	touch $@
endif

env:
	$(CLANG_CHECK)
	env
	echo $(PATH)
	$(GO) version
	which pkg-config
	which brew || true
x86_env:
	arch -arch x86_64 $(MAKE) -w env

rebuild:
	$(MAKE) clean
	$(MAKE)

magic:
	$(MAKE) -w install_pkg_deps
	$(MAKE) -w rebuild

ldd: player/ldd native/ldd
test: player/test

ifeq ($(ENABLE_PKGBUILD),1)
## Only Arch Linux supported for now
  ifneq ($(PACKAGING),1)
    include Makefile.packaging
  endif
else
packaging/clean:
	@true
endif
