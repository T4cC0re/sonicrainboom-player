# SonicRainBoom-player

uses [curl-websocket](https://github.com/barbieri/barbieri-playground/tree/1fc83ba55af8e61478a4060a61cf301094c36b9d/curl-websocket) licensed under the [MIT license](https://github.com/barbieri/barbieri-playground/blob/1fc83ba55af8e61478a4060a61cf301094c36b9d/curl-websocket/LICENSE)

Build requirements:
  - macOS universal binary:
    - Apple Silicon Mac with Rosetta
    - macOS 11+
    - resulting binary will require macOS 10.15+ (x86_64) or macOS 11+ (Apple Silicon)
  - macOS x86 binary
    - Intel Mac
    - macOS 11+
    - resulting binary will require macOS 10.15+
  - Linux
    - anything should be fine.
  - Windows
    - currently unsupported, but support is planned.
