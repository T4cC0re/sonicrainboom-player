package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
)

type Logger struct {
	logrus *logrus.Logger
}

var std Logger

const LOG_QUIET uint32 = uint32(logrus.PanicLevel)
const LOG_FATAL uint32 = uint32(logrus.FatalLevel)
const LOG_ERROR uint32 = uint32(logrus.ErrorLevel)
const LOG_WARN uint32 = uint32(logrus.WarnLevel)
const LOG_INFO uint32 = uint32(logrus.InfoLevel)

func init() {
	std = Logger{
		logrus: logrus.New(),
	}
	if os.Getenv("TRACE") == "1" {
		std.logrus.SetLevel(logrus.TraceLevel)
	} else {
		std.logrus.SetLevel(level)
	}
	Debugf("Set Level %d", level)
}

func SetLevel(level uint32) {
	std.logrus.SetLevel(logrus.Level(level))
}
func (l *Logger) SetLevel(level uint32) {
	l.logrus.SetLevel(logrus.Level(level))
}
func Infof(fmt string, args ...interface{}) {
	std.Infof(fmt, args...)
}
func (l *Logger) Infof(fmt string, args ...interface{}) {
	l.logrus.Infof(fmt, args...)
}
func Info(args ...interface{}) {
	std.Info(args...)
}
func (l *Logger) Info(args ...interface{}) {
	l.logrus.Info(args...)
}
func Errorf(fmt string, args ...interface{}) {
	std.Errorf(fmt, args...)
}
func (l *Logger) Errorf(fmt string, args ...interface{}) {
	l.logrus.Errorf(fmt, args...)
}
func Error(args ...interface{}) {
	std.Error(args...)
}
func (l *Logger) Error(args ...interface{}) {
	l.logrus.Error(args...)
}
func Fatalf(fmt string, args ...interface{}) {
	std.Fatalf(fmt, args...)
}
func (l *Logger) Fatalf(format string, args ...interface{}) {
	panic(fmt.Errorf(format, args...))
	l.logrus.Fatalf(format, args...)
}
func Fatal(args ...interface{}) {
	std.Fatal(args...)
}
func (l *Logger) Fatal(args ...interface{}) {
	panic(fmt.Errorf("%v", args...))
	l.logrus.Fatal(args...)
}
func Warnf(fmt string, args ...interface{}) {
	std.Warnf(fmt, args...)
}
func (l *Logger) Warnf(format string, args ...interface{}) {
	l.logrus.Warnf(format, args...)
}
func Warn(args ...interface{}) {
	std.Warn(args...)
}
func (l *Logger) Warn(args ...interface{}) {
	l.logrus.Warn(args...)
}

func WithField(field string, v interface{}) *Logger {
	return std.WithField(field, v)
}
func (l *Logger) WithField(field string, v interface{}) *Logger {
	return &Logger{logrus: l.logrus.WithField(field, v).Logger}
}
