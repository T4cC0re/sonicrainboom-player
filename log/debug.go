//+build debug

package log

import (
	"github.com/sirupsen/logrus"
)

var level = logrus.DebugLevel

func Debugf(fmt string, args ...interface{}) {
	std.logrus.Debugf(fmt, args...)
}

func (l *Logger) Debugf(fmt string, args ...interface{}) {
	l.logrus.Debugf(fmt, args...)
}

func Debug(args ...interface{}) {
	std.logrus.Debug(args...)
}

func (l *Logger) Debug(args ...interface{}) {
	l.logrus.Debug(args...)
}

func Tracef(fmt string, args ...interface{}) {
	std.logrus.Tracef(fmt, args...)
}

func (l *Logger) Tracef(fmt string, args ...interface{}) {
	l.logrus.Tracef(fmt, args...)
}

func Trace(args ...interface{}) {
	std.logrus.Trace(args...)
}

func (l *Logger) Trace(args ...interface{}) {
	l.logrus.Trace(args...)
}
