//+build !debug

package log

import "github.com/sirupsen/logrus"

var level = logrus.InfoLevel

func Debugf(fmt string, args ...interface{}) {}

func Debug(args ...interface{}) {}

func (l *Logger) Debug(args ...interface{}) {}

func Tracef(fmt string, args ...interface{}) {}

func (l *Logger) Tracef(fmt string, args ...interface{}) {}

func Trace(args ...interface{}) {}

func (l *Logger) Trace(args ...interface{}) {}
