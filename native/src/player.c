//
// Created by t4cc0re on 3/10/21.
//

#include "player.h"
#include "decoder.h"
#include "util.h"
#include <stdlib.h>

#if defined(__APPLE__)
#include "AudioToolbox/AudioToolbox.h"
#endif

pthread_mutex_t stream_cb_buffer_lock;

// the sample callback, running in audio thread
static void stream_cb(float *buffer, int num_frames, int num_channels, void *data) {
    //    SOKOL_ASSERT(2 == num_channels);
    //    SOKOL_ASSERT(SOKOL_AUDIO_ALSA_SAMPLE_SIZE_BYTES == _saudio.bytes_per_frame / _saudio.num_channels);
    pthread_mutex_lock(&stream_cb_buffer_lock);

    static uint16_t count = 0;
    int16_t *       buf   = (void *)buffer;
    for (int i = 0; i < num_frames * num_channels; i++) {
        buf[i] = (count++ & (1 << 3)) ? (int16_t)32768 : (int16_t)-32768;
    }

    pthread_mutex_unlock(&stream_cb_buffer_lock);
}

/**
 * List devices
 * @param buf device_info array for output
 * @param count allocated fields for buf
 * @return amount of devices found and entered into buf
 */
int list_devices(struct device_info *buf, int count) {
#if defined(ASDAD) && (defined(__linux__) || defined(__unix__)) && !defined(__EMSCRIPTEN__) && !defined(__ANDROID__) \
    && !defined(__APPLE__)
    int    index = 0;
    char **hints;
    if (buf == NULL) { return -1; }

    /* Enumerate sound devices */
    int err = snd_device_name_hint(-1, "pcm", (void ***)&hints);
    if (err != 0) { return -1; }

    char **n = hints;
    while (*n != NULL) {
        char *name = snd_device_name_get_hint(*n, "NAME");
        char *desc = snd_device_name_get_hint(*n, "DESC");

        if (name != NULL && 0 != strcmp("null", name) && 0 != strncmp("surround", name, 8)) {
            // Copy name to another buffer and then free it
            if (index < count) {
                buf[index].name = name;
                buf[index].api = srb_sound_api.ALSA;
                if (desc != NULL) {
                    buf[index].desc = desc;
                } else {
                    buf[index].desc = SRB_MALLOC(sizeof(char));
                }
                index++;
            } else {
                // caller must free name, not us.
                // But because this one is not returned, we free it.
                SRB_FREE(name);
                if (desc != NULL) { SRB_FREE(desc); }
            }
        }
        n++;
    } // End of while

    // Free hint buffer too
    //    snd_device_name_free_hint((void **)hints);

    return index;
#elif defined(__APPLE__)
errorf("running on apple\n");

AudioComponent comp;
AudioComponentDescription desc;
AudioComponentInstance auHAL;

//There are several different types of Audio Units.
//Some audio units serve as Outputs, Mixers, or DSP
//units. See AUComponent.h for listing
desc.componentType = kAudioUnitType_Output;

//Every Component has a subType, which will give a clearer picture
//of what this components function will be.
desc.componentSubType = kAudioUnitSubType_HALOutput;

 //all Audio Units in AUComponent.h must use
 //"kAudioUnitManufacturer_Apple" as the Manufacturer
desc.componentManufacturer = kAudioUnitManufacturer_Apple;
desc.componentFlags = 0;
desc.componentFlagsMask = 0;

//Finds a component that meets the desc spec's
comp = AudioComponentFindNext(NULL, &desc);
if (comp == NULL) exit (-1);

    return 0;
#else
    // Not using ALSA.
    // TODO: Implement cross platform
    return 0;
#endif
}

void show_devices() {
    struct device_info *foobar = calloc(128, sizeof(struct device_info));
    int                 devs   = list_devices(foobar, 128);

    for (int i = 0; i < devs; ++i) {
        errorf("re-found dev: %s, Desc:\n%s\n---\n", foobar[i].name, foobar[i].desc);
        SRB_FREE(foobar[i].name);
        SRB_FREE(foobar[i].desc);
    }

    SRB_FREE(foobar);
}

// int init_sound(int sample_rate, int channels, void *device_identifier) {
//    if (!context_ok(active_context) || !saudio_isvalid()) { return -1; }
//
//    saudio_setup(&(saudio_desc){
//        .stream_userdata_cb = stream_cb,
//        .device_identifier  = device_identifier,
//        .sample_rate        = sample_rate,
//        .num_channels       = channels,
//        .user_data          = active_context,
//    });
//
//    if (!saudio_isvalid()) {
//        errorf("failed to init sokol_audio\n");
//        return -1;
//    }
//
//    return 0;
//}

/// LEGACY!!! Remove usages plx.
struct decoder_context *newContext() {
    struct decoder_context *ptr;
    ptr = calloc(1, sizeof(struct decoder_context));

    ptr->cookieA = _SRB_CONTEXT_MAGIC_A;
    ptr->cookieB = _SRB_CONTEXT_MAGIC_B;
    //
    //    pthread_mutex_lock(&active_context_lock);
    //    active_context = ptr;
    //    pthread_mutex_unlock(&active_context_lock);
    //
    //    show_devices();

    return ptr;
}

void srb_shutdown(void) {
    // shutdown sokol-audio
    //    saudio_shutdown();
}
