#include "decoder.h"
#include "resampler.h"
#include "util.h"
#include <assert.h>
#include <jansson.h>

struct decoder_context *active_context = NULL;
pthread_mutex_t         active_context_lock;
bool                    libav_experimental = false;

// TODO free_decoder_context()

/**
 * detect_dsd: detect a DSD stream by context
 *
 * The passed struct is modified.
 *
 * INTERNAL
 *
 * @param ctx   pointer to decoder_context
 */
static void detect_dsd(struct decoder_context *ctx) {
    SRB_PRINTVAL(ctx, "p");

    if (!context_ok(ctx) || !ctx->avCodecContext) { return; }

    uint32_t bitrate_per_channel = ctx->sample_rate * ((uint32_t)ctx->depth) / MAX(ctx->channels, 1);
    switch (ctx->avCodecContext->codec_id) {
        case AV_CODEC_ID_DSD_LSBF_PLANAR:
        case AV_CODEC_ID_DSD_MSBF_PLANAR:
            bitrate_per_channel /= 2;
            // fallthrough
        case AV_CODEC_ID_DSD_LSBF:
        case AV_CODEC_ID_DSD_MSBF:
            if (bitrate_per_channel % 48000 == 0) {
                ctx->dsd = (u_int16_t)(bitrate_per_channel / 48000);
            } else {
                ctx->dsd = (u_int16_t)(bitrate_per_channel / 44100);
            }

            if (ctx->dsd % 64 != 0 || ctx->dsd < 64) { ctx->dsd = 0; }

            break;
        default:
            ctx->dsd = 0;
    }
}

/**
 * fix_depth: fixes sample depth of a context
 *
 * The passed struct is modified.
 *
 * INTERNAL
 *
 * @param ctx   pointer to decoder_context
 */
static void fix_depth(struct decoder_context *ctx) {
    SRB_PRINTVAL(ctx, "p");

    if (!context_ok(ctx) || !ctx->avCodec) { return; }

    switch (ctx->avCodec->id) {
        case AV_CODEC_ID_PCM_S24LE:
        case AV_CODEC_ID_PCM_S24BE:
        case AV_CODEC_ID_PCM_S24LE_PLANAR:
            // There is no AV_CODEC_ID_PCM_S24BE_PLANAR in FFMPEG
            ctx->depth               = 24;
            ctx->override_sample_fmt = AV_SAMPLE_FMT_S32;
            break;
        default:
            if (ctx->depth == 0) { ctx->depth = sample_bits_by_format(ctx->sample_fmt, 0); }
            break;
    }
}

/**
 * fix_depth: fixes sample depth of a context
 *
 * The passed struct is modified.
 *
 * INTERNAL
 *
 * @param ctx   pointer to decoder_context
 * @return AVStream * AVStream pointer or NULL if none found
 */
static AVStream *find_audio_stream(struct decoder_context *ptr) {
    SRB_PRINTVAL(ptr, "p");

    if (!context_ok(ptr)) { return NULL; }
    if (ptr->avFormatContext == NULL) { return NULL; }

    for (unsigned int i = 0; i < ptr->avFormatContext->nb_streams; ++i) {
        if (ptr->avFormatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            return ptr->avFormatContext->streams[i];
        }
    }
    return NULL;
}

/**
 * set_av_flags: Set flags on context for libav
 *
 * INTERNAL
 *
 * @param ptr   pointer to decoder_context
 * @param token optional, token for Bearer auth
 * @return      0 on success, -1 on generic error or libav error code on failure
 * (<0)
 */
static int set_av_flags(struct decoder_context *ptr, const char *token) {
    int ret = 0;

    SRB_PRINTVAL(token, "s");
    SRB_PRINTVAL(ptr, "p");

    if (!context_ok(ptr)) { return -1; }

    if (libav_experimental) {
        const char *setting = "strict";
        const char *value   = "experimental";
        ret                 = av_dict_set(&ptr->avDictionary, setting, value, 0);
        if (ret < 0) { return ret; }
    }

    {
        const char *setting = "user_agent";
        const char *value   = "sonicrainboom-player/" SOURCE_VERSION;
        ret                 = av_dict_set(&ptr->avDictionary, setting, value, 0);
        if (ret < 0) { return ret; }
    }

    if (token != NULL) {
        const char *setting = "user_agent";
        const char *header  = "Authorization: Bearer %s\r\n";
        size_t      n       = strlen(header) + strlen(token) - 2; // Adjust for wasted 2 bytes (%s in format string)
        char *      value   = malloc(n);
        snprintf(value, n, header, token);
        ret = av_dict_set(&ptr->avDictionary, setting, value, 0);
        free(value);
        if (ret < 0) { return ret; }
    }

    return 0;
}

/**
 * change_target_format: Change the target format to be resampled to (if
 * required).
 *
 * @param ctx           pointer to decoder_context
 * @param target_fmt    force a target format (-1/AV_SAMPLE_FMT_NONE to keep
 * existing). NOTE: planar audio will always be changed no packed
 * @param target_depth  force a sample depth (0 to keep existing)
 * @param target_rate   force a rate (0 to keep existing)
 * @param forceStereo   force stereo (false to keep channel layout)
 *
 * @return int          negative on error
 */
int change_target_format(
    struct decoder_context *ctx,
    enum AVSampleFormat     target_fmt,
    u_int8_t                target_depth,
    int32_t                 target_rate,
    bool                    forceStereo) {
    if (!context_ok(ctx)) { return -1; }

    SRB_PRINTVAL(ctx, "p");
    SRB_PRINTVAL(target_fmt, "d");
    SRB_PRINTVAL(target_depth, "d");
    SRB_PRINTVAL(target_rate, "d");
    SRB_PRINTVAL(forceStereo, "d");
    SRB_PRINTVAL(ctx->channels, "d");
    SRB_PRINTVAL(ctx->channel_layout, PRIu64);
    SRB_PRINTVAL(ctx->depth, "d");
    SRB_PRINTVAL(ctx->sample_fmt, "d");
    SRB_PRINTVAL(ctx->sample_rate, "d");
    SRB_PRINTVAL(ctx->swrContext, "p");

    ctx->override_channels       = ctx->channels;
    ctx->override_channel_layout = ctx->channel_layout;
    ctx->override_depth          = ctx->depth;
    ctx->override_sample_fmt     = ctx->sample_fmt;
    ctx->override_sample_rate    = ctx->sample_rate;

    if (av_sample_fmt_is_planar(ctx->override_sample_fmt)) {
        ctx->override_sample_fmt = av_get_alt_sample_fmt(ctx->override_sample_fmt, false);
    }

    if (target_fmt != AV_SAMPLE_FMT_NONE && av_sample_fmt_is_planar(target_fmt)) {
        target_fmt = av_get_alt_sample_fmt(target_fmt, false);
    }

    // This will override the target_fmt. If the input format or target_depth
    // was floating point (and the depth allows) the target_fmt will be floating
    // point again
    if (target_depth > 0) {
        ctx->override_depth = target_depth;
        switch (ctx->override_depth) {
            case 64:
                target_fmt = AV_SAMPLE_FMT_DBL;
                break;
            case 32:
                if (target_fmt == AV_SAMPLE_FMT_DBL || ctx->override_sample_fmt == AV_SAMPLE_FMT_DBL
                    || target_fmt == AV_SAMPLE_FMT_FLT || ctx->override_sample_fmt == AV_SAMPLE_FMT_FLT) {
                    // If the input was supposed to be floating point, make this
                    // flt32
                    target_fmt = AV_SAMPLE_FMT_FLT;
                } else {
                    // Otherwise s32, please
                    target_fmt = AV_SAMPLE_FMT_S32;
                }
                break;
            case 24:
                // Despite only 24-bits being used, libav will handle it in
                // 32-bit with the upper 8 bits == 0x00
                target_fmt = AV_SAMPLE_FMT_S32;
                break;
            case 16:
            default:
                target_fmt = AV_SAMPLE_FMT_S16;
                break;
            case 8:
                target_fmt = AV_SAMPLE_FMT_U8;
                break;
        }
    }

    if (target_rate > 0) {
        ctx->override_sample_rate = target_rate;
    } else {
        ctx->override_sample_rate = ctx->sample_rate;
    }

    if (forceStereo && ctx->channels != 2) {
        ctx->override_channels       = 2;
        ctx->override_channel_layout = AV_CH_LAYOUT_STEREO_DOWNMIX;
    }

    if (target_fmt != AV_SAMPLE_FMT_NONE) {
        // Do not convert formats, but do change the fmt to non-planar in every
        // case.
        ctx->override_sample_fmt = av_get_alt_sample_fmt(target_fmt, false);
    }

    ctx->override_depth = sample_bits_by_format(ctx->override_sample_fmt, ctx->override_depth);

    if (ctx->override_channel_layout != ctx->channel_layout || ctx->override_channels != ctx->channels
        || ctx->override_depth != ctx->depth || ctx->override_sample_fmt != ctx->sample_fmt
        || ctx->override_sample_rate != ctx->sample_rate) {
        if (ctx->swrContext == NULL) {
            ctx->swrContext = swr_alloc();
            if (ctx->swrContext == NULL) {
                errorf("Unable to allocate swr context\n");
                return -1;
            }
        }
    } else {
        if (ctx->swrContext != NULL) { swr_free(&ctx->swrContext); }
    }

    SRB_PRINTVAL(ctx->override_channels, "d");
    SRB_PRINTVAL(ctx->channel_layout, PRIu64);
    SRB_PRINTVAL(ctx->override_depth, "d");
    SRB_PRINTVAL(ctx->override_sample_fmt, "d");
    SRB_PRINTVAL(ctx->override_sample_rate, "d");
    SRB_PRINTVAL(ctx->swrContext, "p");
    SRB_PRINTVAL(ctx->swrContext != NULL, "d");

    return 0;
}

void detect_mqa(struct decoder_context *ptr) {
    int         err;
    int         nb_samples;
    uint64_t    frame_counter;
    uint8_t     frames_without_mqa;
    AVFrame *   frame;
    AVPacket *  packet;
    srb_buffer *buffer;
    assert(context_ok(ptr));

    if (ptr->override_channels != 2 || (ptr->override_sample_rate != 44100 && ptr->override_sample_rate != 48000)
        || (ptr->override_depth != 16 && ptr->override_depth != 24) || ptr->dsd > 0) {
        errorf("Not a qualifying MQA stream (16/24bit, 44100/48000kHz, stereo)\n");
        ptr->mqa_context->finished = true; // Otherwise the MQA analysis will be 'pending'
        return;
    }

    frame = av_frame_alloc();
    assert(frame != NULL);
    packet = av_packet_alloc();
    assert(packet != NULL);
    buffer = srb_buffer_alloc(0);
    assert(srb_buffer_ok(buffer));

    while (av_read_frame(ptr->avFormatContext, packet) >= 0) {
        if (packet->stream_index != ptr->avStream->index) {
            // ignore frames not from the audio stream
            continue;
        }

        err = avcodec_send_packet(ptr->avCodecContext, packet);
        if (err == AVERROR(EAGAIN) || err == AVERROR_EOF) {
            errorf("avcodec_send_packet(): %s\n", av_err2str(err));
            continue;
        } else if (err < 0) {
            errorf("fatal avcodec_send_packet(): %s\n", av_err2str(err));
            return;
        }

        err = avcodec_receive_frame(ptr->avCodecContext, frame);
        if (err == AVERROR(EAGAIN) || err == AVERROR_EOF) {
            errorf("avcodec_receive_frame(): %s\n", av_err2str(err));
            continue;
        } else if (err < 0) {
            errorf("fatal avcodec_receive_frame(): %s\n", av_err2str(err));
            return;
        }

        //        SRB_PRINTVAL(packet, "p");
        //        SRB_PRINTVAL(frame, "p");

        // TODO: implement samples_from_frame
        samples_from_frame(ptr, frame, buffer);
    }

    SRB_FREE(buffer);
    av_frame_free(&frame);
    av_packet_free(&packet);
}

/**
 *
 * @param ctx
 * @param frame
 * @return pointer to buffer (with frame data pointer!) or NULL
 */
static inline srb_buffer *new_resample(struct decoder_context *ctx, AVFrame *frame) {
    // TODO: Implement
    return NULL;
}

/**
 * Copies samples from source to target.
 *
 * If samplesize is 3 bytes, it will skip every 4th byte starting with the
 * first. Otherwise this behaves like `memcpy()`
 *
 * Example: A source buffer is provided with bytes [00,12,34,56,78,9a,bc,de] and
 * sample_size is 3. target_len should be at least 3/4 of source_len. target
 * would contain [12,34,56,9a,bc,de]
 *
 *
 * @param source
 * @param target
 * @param sample_size
 * @return how many bytes were written, or negative on error
 */
int32_t copy_sample(srb_buffer *source, srb_buffer *target, u_int8_t sample_size) {
    int32_t ret = 0;

    if (!srb_buffer_ok(source) || !srb_buffer_ok(target)) { return SRB_CSR_INVALID_BUFFER; }

    pthread_mutex_lock(&source->lock);
    pthread_mutex_lock(&target->lock);

    if (source->len < 0 || target->len < 0) {
        ret = SRB_CSR_TARGET_BUFFER_TOO_SMALL;
        goto ret;
    }

    if (sample_size == 3) {
        int32_t  source_nr_samples = source->len / 4;
        int32_t  target_nr_samples = target->len / 3;
        uint32_t copied_nr_samples = 0;

        if (target_nr_samples < source_nr_samples) {
            ret = SRB_CSR_TOO_FEW_SAMPLES;
            goto ret;
        }

#define source_offset (source->data + (copied_nr_samples * 4 + 1))
#define target_offset (target->data + (copied_nr_samples * 3))

        for (; copied_nr_samples < source_nr_samples; ++copied_nr_samples) {
            // We need to loop, as we need to skip every 4th byte
            memcpy(target_offset, source_offset, 3);
        }
        ret = copied_nr_samples * sample_size;
    } else {
        if (source->len > target->len) {
            ret = SRB_CSR_TARGET_BUFFER_TOO_SMALL;
            goto ret;
        }

        memcpy(target->data, source->data, (size_t)MIN(source->len, target->len));
        ret = MIN(source->len, target->len);
    }

ret:
    pthread_mutex_unlock(&source->lock);
    pthread_mutex_unlock(&target->lock);
    return ret;
}

/**
 *
 * @param ctx
 * @param frame
 * @param buffer
 * @return
 */
int samples_from_frame(struct decoder_context *ctx, AVFrame *frame, srb_buffer *buffer) {
    int         sample_size = ctx->override_depth / 8;
    int         data_size   = 0;
    int         samples     = 0;
    srb_buffer *tmp         = NULL;

    assert(srb_buffer_ok(buffer));

    if (ctx->swrContext != NULL) {
        // Resample
        tmp = new_resample(ctx, frame);
        if (tmp == NULL) { return samples; }
    } else {
        // No resampling
        samples   = frame->nb_samples;
        data_size = av_samples_get_buffer_size(
            NULL,
            frame->channels,
            frame->nb_samples,
            (enum AVSampleFormat)frame->format,
            1);
        if (data_size < 0) { return data_size; }
        // alloc buffer with 0, as we just polyfill the frame data into an srb_buffer.
        tmp       = srb_buffer_alloc(0);
        tmp->len  = (uint32_t)data_size;
        tmp->data = frame->data[0];
    }

    if (sample_size == 3) { data_size = data_size / 4 * 3; }

    if (!srb_buffer_realloc(buffer, (uint32_t)data_size)) {
        // buffer is invalid
        return -1;
    }

    copy_sample(tmp, buffer, (u_int8_t)sample_size);

    return samples;
}

/**
 * initialize_decoder_context: Create a new decoder context and initialize it
 * for use.
 *
 * If a pointer to an existing instance is passed, it will be reset.
 * Any pointer passed currently referenced as the active_context will return an
 * error.
 *
 * @param ptr           pointer to decoder_context
 * @param filename      path to file or URL
 * @param force_rate    force a rate (0 to disable)
 * @param force_depth   force a sample depth (0 to disable)
 * @param force_stereo  force stereo (false to disable)
 * @param dither        dither method to use if required
 * @param resampler     resampler to use if required (soxr or swr) (soxr only if
 * support was compiled in)
 * @param token         optional, token for Bearer auth
 * @return              0 on success, -1 on generic error or libav error code on
 * failure (<0)
 */
int initialize_decoder_context(
    struct decoder_context *ptr,
    const char *            filename,
    int32_t                 force_rate,
    uint8_t                 force_depth,
    bool                    force_stereo,
    const char *            dither,
    const char *            resampler,
    const char *            token) {
    int ret = 0;
    if (ptr == NULL) {
        // null pointer? Nope...
        return -1;
    }
    if (filename == NULL) {
        // null pointer? Nope...
        return -1;
    }

    SRB_PRINTVAL(filename, "s");

    LOCK_ACTIVE_CONTEXT();
    if (ptr == active_context) {
        // refuse to re-init active context
        UNLOCK_ACTIVE_CONTEXT();
        return -1;
    } else {
        UNLOCK_ACTIVE_CONTEXT();
    }

    if (context_ok(ptr)) {
        // When we get an existing one, clear it.
        // TODO FREE struct items
        memset(ptr, 0, sizeof(struct decoder_context));
    }

    ptr->cookieA = _SRB_CONTEXT_MAGIC_A;
    ptr->cookieB = _SRB_CONTEXT_MAGIC_B;

    if (0 != (ret = set_av_flags(ptr, token))) {
        // TODO FREE
        return ret;
    }

    ptr->avFormatContext = avformat_alloc_context();

    if (0 != (ret = avformat_open_input(&ptr->avFormatContext, filename, NULL, &ptr->avDictionary))) {
        // TODO FREE
        return ret;
    }

    if ((ret = avformat_find_stream_info(ptr->avFormatContext, NULL)) > 0) {
        // TODO FREE
        return ret;
    }

    av_dump_format(ptr->avFormatContext, 0, "sonicrainboom-player internal input", 0);

    ptr->avStream = find_audio_stream(ptr);
    if (ptr->avStream == NULL) {
        errorf("No audio stream found\n");
        // TODO FREE
        return -1;
    }

    ptr->avCodec = avcodec_find_decoder(ptr->avStream->codecpar->codec_id);
    if (ptr->avCodec == NULL) {
        errorf("Unable to load decoder\n");
        // TODO FREE
        return -1;
    }

    ptr->avCodecContext = avcodec_alloc_context3(ptr->avCodec);
    if (ptr->avCodecContext == NULL) {
        errorf("Unable to allocate codec context\n");
        // TODO FREE
        return -1;
    }

    if (0 != (ret = avcodec_parameters_to_context(ptr->avCodecContext, ptr->avStream->codecpar))) {
        errorf("Unable to avcodec_parameters_to_context\n");
        // TODO FREE
        return ret;
    }

    ptr->sample_rate    = ptr->avCodecContext->sample_rate;
    ptr->channels       = ptr->avCodecContext->channels;
    ptr->depth          = (u_int8_t)ptr->avCodecContext->bits_per_raw_sample;
    ptr->sample_fmt     = ptr->avCodecContext->sample_fmt;
    ptr->channel_layout = ptr->avCodecContext->channel_layout;
    fix_depth(ptr);
    detect_dsd(ptr);

    char *dsdSuffix  = SRB_MALLOC(12);
    char *fmt_string = SRB_MALLOC(16);
    if (ptr->dsd != 0) {
        errorf("Detected DSD%d\n", ptr->dsd);
        snprintf(dsdSuffix, 12, " DSD%d", ptr->dsd);
    }

    (void)av_get_sample_fmt_string(fmt_string, 16, ptr->sample_fmt);

    errorf(
        "Format determined as %d Hz %d Channels, %d bit/sample, sample_fmt: %s%s\n",
        ptr->sample_rate,
        ptr->channels,
        ptr->depth,
        fmt_string,
        dsdSuffix);
    SRB_FREE(dsdSuffix);
    SRB_FREE(fmt_string);

    if (avcodec_open2(ptr->avCodecContext, ptr->avCodec, &ptr->avDictionary) < 0) {
        errorf("Could not open codec\n");
        // TODO FREE
        return -1;
    }

    errorf("Loaded file '%s' successfully\n", filename);

    change_target_format(ptr, AV_SAMPLE_FMT_NONE, force_depth, force_rate, force_stereo);

    ptr->mqa_context = new_mqa_context();

    // For MQA detection we must not dither. But resampling might be ok, if we
    // just change the sample_fmt
    {
        ptr->dither    = SWR_DITHER_NONE;
        ptr->resampler = SWR_ENGINE_SWR;

        detect_mqa(ptr);
        if (ptr->mqa_context->isMqa) {
            if (ptr->swrContext != NULL) {
                errorf("bit-perfect resampling (sample format conversion) to preserve MQA\n");
            }
        } else {
            // When it turns out the input is not MQA, we can resample with user
            // settings while playback
            ptr->dither    = dither_by_name(dither);
            ptr->resampler = (enum SwrEngine)get_supported_resampler(resampler);
        }
    }

    return 0;
}

const char *stringify_context(struct decoder_context *ctx) {
    json_t *obj = json_object();
    if (!context_ok(ctx)) { json_object_set(obj, "isValid", json_false()); }

    json_object_set(obj, "isValid", json_true());
    json_object_set(obj, "avFormatContext", json_sprintf("%p", ctx->avFormatContext));
    json_object_set(obj, "avStream", json_sprintf("%p", ctx->avStream));
    json_object_set(obj, "avCodecContext", json_sprintf("%p", ctx->avCodecContext));
    json_object_set(obj, "avCodec", json_sprintf("%p", ctx->avCodec));
    json_object_set(obj, "swrContext", json_sprintf("%p", ctx->swrContext));
    json_object_set(obj, "avDictionary", json_sprintf("%p", ctx->avDictionary));
    json_object_set(obj, "mqa_context", json_sprintf("%p", ctx->mqa_context));
    json_object_set(obj, "resample", json_boolean(ctx->swrContext != NULL));
    json_object_set(obj, "dsd", json_integer(ctx->dsd));
    json_object_set(obj, "sample_fmt", json_sprintf("%s", av_get_sample_fmt_name(ctx->sample_fmt)));
    json_object_set(obj, "sample_rate", json_integer(ctx->sample_rate));
    json_object_set(obj, "depth", json_integer(ctx->depth));
    json_object_set(obj, "channels", json_integer(ctx->channels));
    json_object_set(obj, "channel_layout", json_integer((json_int_t)ctx->channel_layout));
    json_object_set(obj, "override_sample_fmt", json_integer(ctx->override_sample_fmt));
    json_object_set(obj, "override_sample_rate", json_integer(ctx->override_sample_rate));
    json_object_set(obj, "override_depth", json_integer(ctx->override_depth));
    json_object_set(obj, "override_channels", json_integer(ctx->override_channels));
    json_object_set(obj, "override_channel_layout", json_integer((json_int_t)ctx->override_channel_layout));
    json_object_set(obj, "dither", json_integer(ctx->dither));
    json_object_set(obj, "resampler", json_integer(ctx->resampler));
    return json_dumps(obj, JSON_SORT_KEYS | JSON_INDENT(2));
}
