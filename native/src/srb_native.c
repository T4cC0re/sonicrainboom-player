#ifdef __APPLE__
#    define MA_NO_RUNTIME_LINKING
#    define MA_ENABLE_ONLY_SPECIFIC_BACKENDS
#    define MA_ENABLE_COREAUDIO
#else
//#define MA_NO_PULSEAUDIO
#endif
#define MA_NO_DECODING
#define MA_NO_ENCODING
#define MA_NO_WAV
#define MA_NO_FLAC
#define MA_NO_MP3
#define MA_NO_GENERATION
#define MA_DEBUG_OUTPUT
#define MINIAUDIO_IMPLEMENTATION

#include <miniaudio.h>

#include "srb_native.h"
#include <curl/curl.h>
#include <portaudio.h>

#ifdef WITH_MBED_TLS
#    include <mbedtls/version.h>
#endif

#include <bzlib.h>
#include <jansson.h>
#include <zlib.h>

extern int list_devices(struct device_info *buf, int count);
/**
 * ONLY use with a constant as `name`!
 * If `name` is not constant, this code creates a potential spot for
 * vulnerabilities!
 */
#define SHOW_VERSION(name)                                                                                  \
    {                                                                                                       \
        /* Flawfinder: ignore CWE-134 */                                                                    \
        fprintf(stderr, #name ":\t%s\t(%s)\n", srb_native_info.name.version, srb_native_info.name.license); \
    }

static inline const char *extract_version(uint32_t version) {
    char *ret = SRB_MALLOC(16);
    snprintf(ret, 16, "%u.%u.%u", (version >> 16) & 0xff, (version >> 8) & 0xff, version & 0xff);
    return ret;
}

static inline const char *format_version(int versionMaj, int versionMin, int versionSub, const char *supplemental) {
    char *ret;
    if (supplemental == NULL) {
        ret = SRB_MALLOC(16);
        snprintf(ret, 16, "%u.%u.%u", versionMaj & 0xff, versionMin & 0xff, versionSub & 0xff);
    } else {
        ret = SRB_MALLOC(strlen(supplemental) + 19);
        snprintf(
            ret,
            strlen(supplemental) + 19,
            "%u.%u.%u (%s)",
            versionMaj & 0xff,
            versionMin & 0xff,
            versionSub & 0xff,
            supplemental);
    }

    return ret;
}

/**
 * Do not free() this variable. You'll crash horribly.
 */
const char *srb_version() { return SOURCE_VERSION; }

const char *avErrorToString(int code) {
    // Need to wrap this, because this is a macro.
    return strdup(av_err2str(code));
}

srb_info srb_native_info;

static void init_srb_native_info() {
    if (srb_native_info.initialized) { return; }

#ifdef WITH_MBED_TLS
    srb_native_info.mbedTLS.version = SRB_MALLOC(64);
    // casting away const, as this is the only place where we modify this.
    mbedtls_version_get_string((char *)srb_native_info.mbedTLS.version);
    srb_native_info.mbedTLS.license = "Apache-2.0"; // GPL3 Compatible
#endif

    srb_native_info.srb.version  = SOURCE_VERSION;
    srb_native_info.srb.license  = "GPL version 3 or later";
    srb_native_info.srb.arch     = get_cpu_arch(true);
    srb_native_info.srb.compiler = __VERSION__;

    srb_native_info.PortAudio.version = format_version(
        Pa_GetVersionInfo()->versionMajor,
        Pa_GetVersionInfo()->versionMinor,
        Pa_GetVersionInfo()->versionSubMinor,
        Pa_GetVersionInfo()->versionControlRevision);
    srb_native_info.PortAudio.license     = "MIT license"; // GPL3 Compatible
    srb_native_info.FFmpeg.version        = av_version_info();
    srb_native_info.FFmpeg.license        = avutil_license();
    srb_native_info.libavcodec.version    = extract_version(avcodec_version());
    srb_native_info.libavcodec.license    = avcodec_license();
    srb_native_info.libavformat.version   = extract_version(avformat_version());
    srb_native_info.libavformat.license   = avformat_license();
    srb_native_info.libavutil.version     = extract_version(avutil_version());
    srb_native_info.libavutil.license     = avutil_license();
    srb_native_info.libswresample.version = extract_version(swresample_version());
    srb_native_info.libswresample.license = swresample_license();
    srb_native_info.bzip2.version         = BZ2_bzlibVersion();
    srb_native_info.bzip2.license         = "bzip2 license"; // GPL3 Compatible
    srb_native_info.zlib.version          = ZLIB_VERSION;
    srb_native_info.zlib.license          = "zlib license"; // GPL3 Compatible
    srb_native_info.libcurl.version       = curl_version_info(CURLVERSION_NOW)->version;
    srb_native_info.libcurl.license       = "curl license"; // GPL3 Compatible
    srb_native_info.initialized           = true;
}

void print_versions(void) {
    init_srb_native_info();

    SRB_PRINTVAL(print_versions, "p");

    fprintf(
        stderr,
        "sonicrainboom-player:\t%s\t(%s)\nCompiler:\t%s\n",
        srb_native_info.srb.version,
        srb_native_info.srb.arch,
        srb_native_info.srb.compiler);
#if defined(__APPLE__)
    errorf("min macOS version: %u\n", __ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__);
#endif
    SHOW_VERSION(PortAudio);
    SHOW_VERSION(libcurl);
    SHOW_VERSION(zlib);
    SHOW_VERSION(bzip2);
#ifdef WITH_MBED_TLS
    SHOW_VERSION(mbedTLS);
#endif
    SHOW_VERSION(FFmpeg);
    SHOW_VERSION(libavcodec);
    SHOW_VERSION(libavformat);
    SHOW_VERSION(libavutil);
    SHOW_VERSION(libswresample);

    //    show_devices();
}

int32_t
fill_buffer(void *source, int32_t source_len, int32_t offset, void *target, int32_t target_cap, int32_t written) {
    int32_t target_cap_adjusted = MIN(target_cap, target_cap - written);
    int32_t copy_len            = MIN(source_len - offset, target_cap_adjusted);

    if (source_len < offset) {
        // offset overreads source bounds
        return 0;
    }

    if (source_len - offset > target_cap) {
        // request too big
        return 0;
    }

    if (target_cap_adjusted <= 0) {
        // buffer already full
        return 0;
    }

    memcpy(target + written, source + offset, (size_t)copy_len);

    return copy_len;
}

static inline bool swap_active_context(struct decoder_context *new_context) {
    LOCK_ACTIVE_CONTEXT();
    SRB_PRINTVAL(active_context, "p");

    if (active_context != NULL) {
        UNLOCK_ACTIVE_CONTEXT();
        return false;
    } else {
        active_context = new_context;
        UNLOCK_ACTIVE_CONTEXT();
        return true;
    }
}

static inline ma_format ma_format_by_libav(int32_t av_sample_fmt, uint8_t depth) {
    switch (av_sample_fmt) {
        case AV_SAMPLE_FMT_U8:
        case AV_SAMPLE_FMT_U8P:
            return ma_format_u8;
        case AV_SAMPLE_FMT_S16:
        case AV_SAMPLE_FMT_S16P:
            return ma_format_s16;
        case AV_SAMPLE_FMT_S32:
        case AV_SAMPLE_FMT_S32P:
            return depth == 3 ? ma_format_s24 : ma_format_s32;
        case AV_SAMPLE_FMT_FLT:
        case AV_SAMPLE_FMT_FLTP:
            return ma_format_f32;
        default:
            return ma_format_unknown;
    }
}

void data_callback(ma_device *pDevice, void *pOutput, const void *pInput, ma_uint32 frameCount) {
    LOCK_ACTIVE_CONTEXT();

end:
    UNLOCK_ACTIVE_CONTEXT();
}

static void my_log_callback(ma_context *pContext, ma_device *pDevice, ma_uint32 logLevel, const char *message) {
    SRB_PRINTVAL(pContext, "p");
    SRB_PRINTVAL(pDevice, "p");
    SRB_PRINTVAL(logLevel, "d");
    SRB_PRINTVAL(message, "s");
}

static inline void list_all_outputs(const char *devicename, ma_backend* out_selected_backend, ma_device_id* out_selected_device_id) {
    ma_result       result;
    ma_context      context;
    ma_device_info *pPlaybackDeviceInfos = NULL;
    ma_uint32       playbackDeviceCount;
    ma_uint32       iDevice;
    ma_device       device;
    ma_backend      backends[] = {
        #if defined(MA_HAS_COREAUDIO)
        ma_backend_coreaudio,
        #endif
        #if defined(MA_HAS_JACK)
        ma_backend_jack,
        #endif
        #if defined(MA_HAS_PULSEAUDIO)
        ma_backend_pulseaudio,
        #endif
        #if defined(MA_HAS_ALSA)
        ma_backend_alsa,
        #endif
        #if defined(MA_HAS_WASAPI)
        ma_backend_wasapi,
        #endif
        #if defined(MA_HAS_DSOUND)
        ma_backend_dsound,
        #endif
        #if defined(MA_HAS_SNDIO)
        ma_backend_sndio,
        #endif
        #if defined(MA_HAS_AUDIO4)
        ma_backend_audio4,
        #endif
        #if defined(MA_HAS_OSS)
        ma_backend_oss,
        #endif
        #if defined(MA_HAS_AAUDIO)
        ma_backend_aaudio,
        #endif
        #if defined(MA_HAS_OPENSL)
        ma_backend_opensl,
        #endif
        #if defined(MA_HAS_WEBAUDIO)
        ma_backend_webaudio,
        #endif
        #if defined(MA_HAS_NULL)
        ma_backend_null,
        #endif
        #if defined(MA_HAS_CUSTOM)
        ma_backend_custom,
        #endif
        255, // This is an array stop marker
      };
    json_t *obj = json_object();

/////

errorf("Supported APIs on your machine:\n");

    #if defined(MA_HAS_COREAUDIO)
    errorf("ma_backend_coreaudio,\n");
    #endif
    #if defined(MA_HAS_JACK)
    errorf("ma_backend_jack,\n");
    #endif
    #if defined(MA_HAS_PULSEAUDIO)
    errorf("ma_backend_pulseaudio,\n");
    #endif
    #if defined(MA_HAS_ALSA)
    errorf("ma_backend_alsa,\n");
    #endif
    #if defined(MA_HAS_WASAPI)
    errorf("ma_backend_wasapi,\n");
    #endif
    #if defined(MA_HAS_DSOUND)
    errorf("ma_backend_dsound,\n");
    #endif
    #if defined(MA_HAS_SNDIO)
    errorf("ma_backend_sndio,\n");
    #endif
    #if defined(MA_HAS_AUDIO4)
    errorf("ma_backend_audio4,\n");
    #endif
    #if defined(MA_HAS_OSS)
    errorf("ma_backend_oss,\n");
    #endif
    #if defined(MA_HAS_AAUDIO)
    errorf("ma_backend_aaudio,\n");
    #endif
    #if defined(MA_HAS_OPENSL)
    errorf("ma_backend_opensl,\n");
    #endif
    #if defined(MA_HAS_WEBAUDIO)
    errorf("ma_backend_webaudio,\n");
    #endif
    #if defined(MA_HAS_NULL)
    errorf("ma_backend_null,\n");
    #endif
    #if defined(MA_HAS_CUSTOM)
    errorf("ma_backend_custom,\n");
    #endif

/////



    errorf("sizeof(backends) %lu.\n", sizeof(backends));


    for (int c = 0; c < sizeof(backends) && backends[c] != 255; c++) {
        json_t *backend = json_object();
        json_t *devices = json_object();
        errorf("c = %d, %s\n", c, ma_get_backend_name(backends[c]));
        json_object_set(backend, "name", json_string(ma_get_backend_name(backends[c])));

        ma_context_config ctx_config                = ma_context_config_init();
        ctx_config.logCallback                      = my_log_callback;
        ctx_config.alsa.useVerboseDeviceEnumeration = true;

        if (ma_context_init(&backends[c], 1, &ctx_config, &context) != MA_SUCCESS) {
            errorf("Failed to initialize context.\n");
            goto free;
        }

        result = ma_context_get_devices(&context, &pPlaybackDeviceInfos, &playbackDeviceCount, NULL, NULL);

        if (result != MA_SUCCESS) {
            errorf("Failed to retrieve device information.\n");
            goto free;
        }

        errorf("Playback Devices list_all_outputs\n");
        for (iDevice = 0; iDevice < playbackDeviceCount; ++iDevice) {
            json_t *json_device = json_object();
            json_t *formats     = json_array();

            // more fields require more querying:
            ma_context_get_device_info(
                &context,
                ma_device_type_playback,
                &pPlaybackDeviceInfos[iDevice].id,
                ma_share_mode_exclusive,
                //                    ma_share_mode_shared,
                &pPlaybackDeviceInfos[iDevice]);

            json_object_set(json_device, "is_default", json_boolean(pPlaybackDeviceInfos[iDevice].isDefault));

bool is_selected = (devicename == NULL && pPlaybackDeviceInfos[iDevice].isDefault) || (devicename != NULL && 0 == strcmp(pPlaybackDeviceInfos[iDevice].name, devicename));

            json_object_set(
                json_device,
                "is_selected",
                json_boolean(is_selected));

            json_object_set(json_device, "name", json_string(pPlaybackDeviceInfos[iDevice].name));

            if (is_selected) {
              *out_selected_backend = backends[c];
              *out_selected_device_id = pPlaybackDeviceInfos[iDevice].id;
            }

            for (int i = 0; i < pPlaybackDeviceInfos[iDevice].nativeDataFormatCount; ++i) {
                json_array_append(
                    formats,
                    json_sprintf(
                        "%s/%d",
                        ma_format_string(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].format),
                        pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].sampleRate));
            }

            json_object_set(json_device, "formats", formats);
            json_object_set(devices, pPlaybackDeviceInfos[iDevice].name, json_device);
        }
        json_object_set(backend, "devices", devices);
        json_object_set(obj, ma_get_backend_name(backends[c]), backend);

    free:
        ma_device_uninit(&device);
        ma_context_uninit(&context);
    }

    errorf("%s\n", json_dumps(obj, JSON_SORT_KEYS | JSON_INDENT(2)));
}

/**
 * Used by Go to handoff everything to C
 * @param filename
 * @param force_rate
 * @param force_depth
 * @param force_stereo
 * @param dither
 * @param resampler
 * @param devicename
 * @param token
 */
void handoff(
    const char *filename,
    int32_t     force_rate,
    uint8_t     force_depth,
    bool        force_stereo,
    const char *dither,
    const char *resampler,
    const char *devicename,
    const char *token) {
    ma_result        result;
    ma_context       context;
    ma_device_info * pPlaybackDeviceInfos = SRB_MALLOC(sizeof(ma_device_info));
    // ma_uint32        playbackDeviceCount;
    ma_uint32        iDevice;
    int32_t          chosenDevice = -1;
    ma_device_config config;
    ma_device        device;
    void* user_data = NULL;

    assert(force_rate % 44100 == 0 || force_rate % 48000 == 0);
    assert(force_depth % 8 == 0);

    // region device enumeration and  device matching

    struct device_info *buf = SRB_MALLOC(sizeof(struct device_info) * 64);
list_devices(buf, 64);


    ma_device_id selected_device_id;
    selected_device_id.nullbackend = 0;
    ma_backend selected_backend = ma_backend_null;

    list_all_outputs(devicename, &selected_backend, &selected_device_id);
    errorf("selected_device_id %d, selected_backend %d\n", selected_device_id, selected_backend);

    // ensure we have a device selected
    assert(selected_device_id.nullbackend != 0);
    errorf("----------------------------------------\n\n\n\n\n\n\n\n\n\n\n\n----------------------------------------");
//
// #if defined(__APPLE__)
//     ma_backend backends[] = {ma_backend_coreaudio};
// #else
//     ma_backend backends[] = {ma_backend_alsa, ma_backend_pulseaudio, ma_backend_wasapi, ma_backend_dsound};
// #endif
//
    ma_context_config ctx_config                = ma_context_config_init();
    ctx_config.pUserData = user_data;
    ctx_config.logCallback                      = my_log_callback;
    ctx_config.alsa.useVerboseDeviceEnumeration = true;
    ctx_config.pulse.pApplicationName = "SonicRainBoom-player";
    ctx_config.jack.pClientName = "SonicRainBoom-player";
    ctx_config.coreaudio.sessionCategory = ma_ios_session_category_playback; // iOS only

    if (ma_context_init(&selected_backend, 1, &ctx_config, &context) != MA_SUCCESS) {
        errorf("Failed to initialize context.\n");
        exit(2);
    }
//
    // result = ma_context_get_devices(&context, pPlaybackDeviceInfos, &playbackDeviceCount, NULL, NULL);

#define PLAYBACK_MODE ma_share_mode_shared


          result =  ma_context_get_device_info(
                &context,
                ma_device_type_playback,
                &selected_device_id,
                PLAYBACK_MODE,
                pPlaybackDeviceInfos);

//
    if (result != MA_SUCCESS) {
        errorf("Failed to retrieve device information.\n");
        exit(3);
    }
//
//     errorf("Playback Devices\n");
//     for (iDevice = 0; iDevice < playbackDeviceCount; ++iDevice) {
//         //        SRB_PRINTVAL(iDevice, "u");
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].name, "s");
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].isDefault, "d");
//
//         // more fields require more querying:
//         ma_context_get_device_info(
//             &context,
//             ma_device_type_playback,
//             &pPlaybackDeviceInfos[iDevice].id,
//             ma_share_mode_exclusive,
//             //                    ma_share_mode_shared,
//             &pPlaybackDeviceInfos[iDevice]);
//
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].formatCount, "d");
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].minChannels, "d");
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].maxChannels, "d");
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].minSampleRate, "d");
//         //        SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].maxSampleRate, "d");
//         //        SRB_PRINTVAL(&pPlaybackDeviceInfos[iDevice], "p");
//
//         if ((devicename == NULL && pPlaybackDeviceInfos[iDevice].isDefault)
//             || (devicename != NULL && 0 == strcmp(pPlaybackDeviceInfos[iDevice].name, devicename))) {
//             chosenDevice = iDevice;
//             errorf(" * ");
//         } else {
//             errorf("   ");
//         }
//
//         errorf("%s:\n    ", pPlaybackDeviceInfos[iDevice].name);
//
//         //        for (int i = 0; i < pPlaybackDeviceInfos[iDevice].formatCount; ++i) {
//         //            SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].formats[i], "d");
//         //            SRB_PRINTVAL(ma_format_string(pPlaybackDeviceInfos[iDevice].formats[i]), "s");
//         //        }
//
//         for (int i = 0; i < pPlaybackDeviceInfos[iDevice].nativeDataFormatCount; ++i) {
//             errorf(
//                 " %s/%d",
//                 ma_format_string(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].format),
//                 pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].sampleRate);
//             //            SRB_PRINTVAL(ma_format_string(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].format),
//             //            "s"); SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].format, "x");
//             //            SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].channels, "d");
//             //            SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].sampleRate, "d");
//             //            SRB_PRINTVAL(pPlaybackDeviceInfos[iDevice].nativeDataFormats[i].flags, "d");
//         }
//
//         errorf("\n");
//
//         //        SRB_PRINTVAL(chosenDevice, "d");
//     }
//     // endregion

    // assert(chosenDevice >= 0);

    /**
     * After this region, we can access `active_context`.
     */
    // region create and set decoder context
    struct decoder_context *new_context = SRB_MALLOC(sizeof(struct decoder_context));
    int                     ret         = 0;

    SRB_PRINTVAL(filename, "s");
    SRB_PRINTVAL(force_rate, "d");
    SRB_PRINTVAL(force_depth, "d");
    SRB_PRINTVAL(force_stereo, "d");
    SRB_PRINTVAL(dither, "s");
    SRB_PRINTVAL(resampler, "s");
    SRB_PRINTVAL(devicename, "s");
    SRB_PRINTVAL(token, "p");
    SRB_PRINTVAL(new_context, "p");
    assert(new_context != NULL);

    libav_experimental = false; // TODO: Have a flag for this

    ret = initialize_decoder_context(
        new_context,
        filename,
        force_rate,
        force_depth,
        force_stereo,
        dither,
        resampler,
        token);

    if (ret < 0) {
        SRB_PRINTVAL(av_err2str(ret), "s");
        ma_context_uninit(&context);
        exit(ret);
    }

    SRB_PRINTVAL(active_context, "p");
    SRB_PRINTVAL(new_context, "p");

    if (!swap_active_context(new_context)) { errorf("Unable to swap context\n"); }

    SRB_PRINTVAL(active_context, "p");
    SRB_PRINTVAL(new_context, "p");
    // endregion

    // region initialize sound device
    bool found_fmt = false;
    config         = ma_device_config_init(ma_device_type_playback);

    LOCK_ACTIVE_CONTEXT();
    for (int i = 0; i < pPlaybackDeviceInfos->nativeDataFormatCount; ++i) {
        SRB_PRINTVAL(ma_format_string(pPlaybackDeviceInfos->nativeDataFormats[i].format), "s");
        SRB_PRINTVAL(pPlaybackDeviceInfos->nativeDataFormats[i].format, "d");
        SRB_PRINTVAL(pPlaybackDeviceInfos->nativeDataFormats[i].channels, "d");
        SRB_PRINTVAL(pPlaybackDeviceInfos->nativeDataFormats[i].sampleRate, "d");
        SRB_PRINTVAL(pPlaybackDeviceInfos->nativeDataFormats[i].flags, "d");

        // TODO: This bugs, when only one parameter is overidden
        if (pPlaybackDeviceInfos->nativeDataFormats[i].format
                == ma_format_by_libav(active_context->override_sample_fmt, active_context->override_depth)
            && (pPlaybackDeviceInfos->nativeDataFormats[i].channels == active_context->override_channels
                || pPlaybackDeviceInfos->nativeDataFormats[i].channels == 0)
            && pPlaybackDeviceInfos->nativeDataFormats[i].sampleRate
                   == active_context->override_sample_rate) {
            config.playback.format = ma_format_by_libav(
                active_context->override_sample_fmt,
                active_context->override_depth);
            config.playback.channels = (ma_uint32)active_context->override_channels;
            config.sampleRate        = (ma_uint32)active_context->override_sample_rate;
            found_fmt                = true;
            break;
        }
    }

    SRB_PRINTVAL(found_fmt, "d");

    if (!found_fmt) {
        if (force_rate != 0 || force_depth != 0 || force_stereo == true) {
            errorf("forced settings do not match device capabilities.\n");
            errorf("%s\n", stringify_context(active_context));
            ma_context_uninit(&context);
            exit(2);
        }

        assert(pPlaybackDeviceInfos->formatCount > 0);
        errorf("!found_fmt\n");
        SRB_PRINTVAL(pPlaybackDeviceInfos->formats[0], "d");
        SRB_PRINTVAL(ma_format_string(pPlaybackDeviceInfos->formats[0]), "s");

        change_target_format(
            active_context,
            ma_format_to_libav(pPlaybackDeviceInfos->formats[0]),
            bit_depth_from_ma_format(pPlaybackDeviceInfos->formats[0]),
            WITHIN_BOUNDS(
                pPlaybackDeviceInfos->minSampleRate,
                active_context->override_sample_rate,
                pPlaybackDeviceInfos->maxSampleRate),
            pPlaybackDeviceInfos->maxChannels >= 2 ? true : false);

        config.playback.format = ma_format_by_libav(
            active_context->override_sample_fmt,
            active_context->override_depth);
        config.playback.channels = (ma_uint32)active_context->override_channels;
        config.sampleRate        = (ma_uint32)active_context->override_sample_rate;
    }

    errorf("%s\n", stringify_context(active_context));
    UNLOCK_ACTIVE_CONTEXT();

    config.dataCallback                           = data_callback;
    config.coreaudio.allowNominalSampleRateChange = true;
    config.playback.pDeviceID                     = &pPlaybackDeviceInfos->id;
    config.playback.shareMode                     = PLAYBACK_MODE;
    config.noClip                                 = 1;
    config.pUserData = user_data;
    config.pulse.pStreamNamePlayback = "SonicRainBoom-Player";

    if (ma_device_init(&context, &config, &device) != MA_SUCCESS) {
        errorf("Failed to open playback device.\n");
        ma_context_uninit(&context);
        exit(2);
    }

    if (ma_device_start(&device) != MA_SUCCESS) {
        errorf("Failed to start playback device.\n");
        ma_device_uninit(&device);
        ma_context_uninit(&context);
        exit(3);
    }
    // endregion

    // TODO: use context to actually decode and write audio to the device

    ma_device_uninit(&device);
    ma_context_uninit(&context);

    SRB_FREE(pPlaybackDeviceInfos);
}
