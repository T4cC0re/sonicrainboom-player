#include "mqa.h"
#include "util.h"

const char *mqa_to_string(struct mqa_context *ctx) {
    char *buf;
    buf = SRB_MALLOC(96);
    if (ctx == NULL) {
        snprintf(buf, 96, "{invalid context}");
    } else {
        if (!ctx->finished) {
            snprintf(buf, 96, "analyzing");
        } else if (ctx->isMqaStudio) {
            snprintf(buf, 96, "MQA Studio (%d-bit %d Hz)", ctx->depth, ctx->originalSampleRate);
        } else if (ctx->isMqa) {
            snprintf(buf, 96, "MQA (%d-bit %d Hz)", ctx->depth, ctx->originalSampleRate);
        } else {
            snprintf(buf, 96, "no MQA");
        }
    }

    return buf;
}

static inline void set_original_sample_rate(struct mqa_context *context, uint8_t code) {
    uint32_t base, multiplier;

    /*
     * The least significant bit signifies the used base (44100 Hz (0) or 48000
     * Hz (1) The next 3 bits are the multiplier. This needs to be unmangled,
     * and in case of DSD multiplied by 2 itself. In the end the base and the
     * multiplier will produce the original sample rate.
     */
    if ((code & 1) == 1) {
        base = 48000;
    } else {
        base = 44100;
    }

    multiplier = (uint32_t)(1 << (((code >> 3) & 1) | (((code >> 2) & 1) << 1) | (((code >> 1) & 1) << 2)));

    /**
     * At this point we reached a higher sample rate than 705600 Hz, which
     * signalizes, that the MQA file was encoded from a DSD source. We double
     * the multiplied to get to DSD sample rates.
     */
    if (multiplier > 16) { multiplier *= 2; }

    /**
     * The code seems to have 5 bits. It's unclear what this bit does. But since
     * sample rates calculated the "old" way only go up to DSD256, the 5th bit
     * seems to be a flag to
     * 1. switch to a different calculation altogether
     * 2. double the rate
     * Number 2 seems to be a viable option to implement until we know more.
     */
    if ((code & 0b00010000) == 0b00010000) {
        // DSD 512? Maybe?
        multiplier *= 2;
    }

    context->originalSampleRate = base * multiplier;
};

#define buffer_offset(sample, depth)      ((uint64_t)(sample) * (((uint64_t)(depth)) / 8) * 2)
#define addr_left(buffer, sample, depth)  ((buffer) + buffer_offset(sample, depth))
#define addr_right(buffer, sample, depth) ((buffer) + buffer_offset(sample, depth) + (((uint64_t)(depth)) / 8))

static inline uint64_t hidden_bit_from_sample(uint8_t depth, uint64_t sample, void *buffer) {
    uint64_t left, right;

    left  = *((uint64_t *)addr_left(buffer, sample, depth));
    right = *((uint64_t *)addr_right(buffer, sample, depth));

    return ((left ^ right) >> (depth - 16)) & 1;
}

struct mqa_context *new_mqa_context() {
    return SRB_MALLOC(sizeof(struct mqa_context));
}

int analyze_samples(
    struct mqa_context *context,
    uint32_t            samples,
    void *              buffer,
    uint32_t            buffer_size,
    uint8_t             depth,
    int                 channels) {
    uint64_t sample = 0;

    SRB_PRINTVAL(context, "p");
    SRB_PRINTVAL(samples, "d");
    SRB_PRINTVAL(buffer, "p");
    SRB_PRINTVAL(buffer_size, "d");
    SRB_PRINTVAL(depth, "d");
    SRB_PRINTVAL(channels, "d");

    if (context == NULL) { return ERR_NO_CONTEXT; }

    if (depth < 16 || depth > 24) {
        // log.Error("AnalyzeSamples called on non 16/24-bit stream")
        return ERR_DEPTH;
    }

    if (channels != 2) {
        // log.Error("AnalyzeSamples called on stream with other than 2
        // channels")
        return ERR_CHANNELS;
    }

    if (buffer == NULL) {
        // log.Error("AnalyzeSamples called without a buffer")
        return ERR_NO_BUFF;
    }

    if (buffer_size < (uint32_t)samples * (uint32_t)((depth) / 8) * (uint32_t)channels) { return ERR_SMALL_BUFF; }

    for (; sample < samples; ++sample) {
        context->accumulator |= hidden_bit_from_sample(depth, sample, buffer);

        if (context->accumulator == 0b101111100000010010011000110010001000) { // We arrived at the
                                                                              // MQA marker
            errorf("MQA marker found in samples at sample %llu (of %d in buffer)\n", sample, samples);

            // Original Sample Rate
            {
                uint8_t  codedSampleRate = 0;
                uint64_t sampleOffset    = 3;
                for (; sampleOffset < 8; ++sampleOffset) {
                    codedSampleRate |= (uint8_t)(hidden_bit_from_sample(depth, sample + (uint64_t)sampleOffset, buffer))
                                       << (6 - sampleOffset);
                }
                set_original_sample_rate(context, codedSampleRate);
            }

            // Provenance Code
            {
                uint64_t sampleOffset = 3;
                for (; sampleOffset < 8; ++sampleOffset) {
                    context->provenance
                        |= (uint8_t)(hidden_bit_from_sample(depth, sample + (uint64_t)sampleOffset, buffer))
                           << (33 - sampleOffset);
                }

                context->isMqaStudio = context->provenance > 8;
            }

            context->depth    = depth;
            context->isMqa    = true;
            context->finished = true;

            goto end;
        }

        context->accumulator = (context->accumulator << 1) & 0xFFFFFFFFF;
    }

end:
    SRB_PRINTVAL(context->originalSampleRate, "d");
    SRB_PRINTVAL(context->isMqa, "d");
    SRB_PRINTVAL(context->finished, "d");
    SRB_PRINTVAL(context->isMqaStudio, "d");
    SRB_PRINTVAL(context->provenance, "d");
    SRB_PRINTVAL(context->depth, "d");
    SRB_PRINTVAL(context->accumulator, PRIu64);

    return ERR_NONE;
}
