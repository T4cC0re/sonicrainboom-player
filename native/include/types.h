//
// Created by t4cc0re on 3/26/21.
//

#ifndef NATIVE_TYPES_H
#define NATIVE_TYPES_H

#include <sys/types.h>
#include <pthread.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>
#include <stdbool.h>

#define _SRB_CONTEXT_MAGIC_A 0xd0d0cafe
#define _SRB_CONTEXT_MAGIC_B 0xdeadbeef

struct decoder_context {
    int                 cookieA;
    AVFormatContext *   avFormatContext;
    AVStream *          avStream;
    AVCodecContext *    avCodecContext;
    AVCodec *           avCodec;
    SwrContext *        swrContext;
    AVDictionary *      avDictionary;
    u_int16_t           dsd;
    enum AVSampleFormat sample_fmt;
    int32_t             sample_rate;
    u_int8_t            depth;
    int32_t             channels;
    u_int64_t           channel_layout;
    enum AVSampleFormat override_sample_fmt;
    int32_t             override_sample_rate;
    u_int8_t            override_depth;
    int32_t             override_channels;
    u_int64_t           override_channel_layout;
    enum SwrDitherType  dither;
    enum SwrEngine      resampler;
    struct mqa_context *mqa_context;
    int                 cookieB;
};
// TODO: Replace `struct decoder_context` with `decoder_context` everywhere
typedef struct decoder_context decoder_context;

typedef struct srb_buffer {
    int             cookieA;
    void *          data;
    uint32_t        len;
    pthread_mutex_t lock;
    void *          self;
} srb_buffer;

typedef struct {
    const char *version;
    const char *license;
} lib_info;

struct srb_info {
    lib_info libcurl;
    lib_info PortAudio;
    struct srb {
        const char *version;
        const char *license;
        const char *arch;
        const char *compiler;
    } srb;
    lib_info zlib;
    lib_info bzip2;
    lib_info FFmpeg;
    lib_info libavcodec;
    lib_info libavformat;
    lib_info libavutil;
    lib_info libswresample;
#ifdef WITH_MBED_TLS
    lib_info mbedTLS;
#endif
    bool initialized;
};

typedef struct srb_info srb_info;

#endif // NATIVE_TYPES_H
