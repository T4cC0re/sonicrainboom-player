#ifndef NATIVE_MACROS_H
#define NATIVE_MACROS_H

#include <stdio.h>

// Make sure all printf is redirected to stderr.
#undef printf
#define printf(...) \
    _Pragma("GCC warning \"printf is discouraged, use errorf or fprintf explicitly\"") fprintf(stderr, __VA_ARGS__)
#define errorf(...) fprintf(stderr, __VA_ARGS__)

#ifdef SRB_NO_TRACE
#    define SRB_PRINTVAL_PREFIX(val, print_type, prefix) \
        {}
#    define SRB_PRINTVAL_PREFIX2(val, print_type, prefix) \
        {}
#    define SRB_PRINTVAL(val, print_type) \
        {}
#else
#    define SRB_PRINTVAL_PREFIX(val, print_type, prefix) \
        errorf(__FILE__ ":%d\t(%s) >>\t" #val ":\t%" print_type "\n", __LINE__, prefix, val)
#    define SRB_PRINTVAL_PREFIX2(val, print_type, prefix) \
        errorf(__FILE__ ":%d\t(%s::%s) >>\t" #val ":\t%" print_type "\n", __LINE__, __FUNCTION__, prefix, val)
#    define SRB_PRINTVAL(val, print_type) SRB_PRINTVAL_PREFIX(val, print_type, __FUNCTION__)
#endif

#define MIN(X, Y)                      (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y)                      (((X) > (Y)) ? (X) : (Y))
#define WITHIN_BOUNDS(min, check, max) (check > min ? (check <= max ? check : max) : min)

// Use zero-initialized calloc rather than malloc
#define SRB_MALLOC_NO_TRACE(size)        SRB_CALLOC_NO_TRACE(1, size)
#define SRB_CALLOC_NO_TRACE(count, size) calloc(count, (size_t)(size))
// Free and set to NULL, but only if the incoming pointer is not NULL already
#define SRB_FREE_NO_TRACE(ptr) \
    {                          \
        if ((ptr) != NULL) {   \
            free(ptr);         \
            (ptr) = NULL;      \
        }                      \
    }

#ifdef SRB_NO_TRACE
#    define SRB_MALLOC SRB_MALLOC_NO_TRACE
#    define SRB_CALLOC SRB_CALLOC_NO_TRACE
#    define SRB_FREE   SRB_FREE_NO_TRACE
#else
#    define SRB_MALLOC(size)                                             \
        ({                                                               \
            void *pointer;                                               \
            SRB_PRINTVAL_PREFIX2((size_t)(size), PRIuPTR, "SRB_MALLOC"); \
            pointer = SRB_MALLOC_NO_TRACE(size);                         \
            SRB_PRINTVAL_PREFIX2(pointer, "p", "SRB_MALLOC");            \
            pointer;                                                     \
        })
#    define SRB_CALLOC(count, size)                                      \
        ({                                                               \
            void *pointer;                                               \
            SRB_PRINTVAL_PREFIX2(count, "d", "SRB_CALLOC");              \
            SRB_PRINTVAL_PREFIX2((size_t)(size), PRIuPTR, "SRB_CALLOC"); \
            pointer = SRB_CALLOC_NO_TRACE(count, size);                  \
            SRB_PRINTVAL_PREFIX2(pointer, "p", "SRB_CALLOC");            \
            pointer;                                                     \
        })
#    define SRB_FREE(ptr)                                     \
        SRB_PRINTVAL_PREFIX2((void *)(ptr), "p", "SRB_FREE"); \
        SRB_FREE_NO_TRACE(ptr)
#endif

#define LOCK_ACTIVE_CONTEXT()   pthread_mutex_lock(&active_context_lock)
#define UNLOCK_ACTIVE_CONTEXT() pthread_mutex_unlock(&active_context_lock)

#endif // NATIVE_MACROS_H
