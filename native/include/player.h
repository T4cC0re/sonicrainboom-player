//
// Created by t4cc0re on 3/10/21.
//

#ifndef NATIVE_PLAYER_H
#define NATIVE_PLAYER_H
#include <stdlib.h>

enum srb_sound_api {
  ALSA = 0,
  CORE_AUDIO = 1,
};

struct device_info {
    char *name;
    char *desc;
    u_int8_t api;
};

struct decoder_context *newContext();

void srb_shutdown(void);

void show_devices(void);

#endif // NATIVE_PLAYER_H
