#ifndef SRB_NATIVE_HEADER
#define SRB_NATIVE_HEADER

#include "mqa.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
//#include "connect.h"
#include "decoder.h"
#include "player.h"
#include "util.h"

/**
 This is only temporary, while we still need to access some fields directly in
 Go To make handling easier, this treats all external pointers as void*.
 **/
struct external_decoder_context {
    int       cookieA;
    void *    avFormatContext;
    void *    avStream;
    void *    avCodecContext;
    void *    avCodec;
    void *    swrContext;
    void *    avDictionary;
    bool      resample;
    u_int16_t dsd;
    int32_t   sample_fmt;
    int32_t   sample_rate;
    u_int8_t  depth;
    int32_t   channels;
    u_int64_t channel_layout;
    int32_t   override_sample_fmt;
    int32_t   override_sample_rate;
    u_int8_t  override_depth;
    int32_t   override_channels;
    u_int64_t override_channel_layout;
    int       cookieB;
};

void print_versions();

const char *srb_version();

const char *avErrorToString(int code);

void handoff(
    const char *filename,
    int32_t     force_rate,
    uint8_t     force_depth,
    bool        force_stereo,
    const char *dither,
    const char *resampler,
    const char *devicename,
    const char *token);

int32_t
fill_buffer(void *source, int32_t source_len, int32_t offset, void *target, int32_t target_cap, int32_t written);

// void
// set_on_context(struct decoder_context *ctx, void *avFormatContext, void
// *avStream, void *avCodecContext, void *avCodec,
//               void *swrContext, void *avDictionary);

// void set_overrides(struct decoder_context *ctx, bool resample, int32_t
// override_sample_fmt, int32_t override_sample_rate,
//                   u_int8_t override_depth, int32_t override_channels,
//                   u_int32_t override_channel_layout);

// int set_dither_settings(struct decoder_context *ctx, int32_t dither, int32_t
// resampler);

// void change_target_format(struct decoder_context *ctx, int32_t target_fmt,
// u_int8_t target_depth, int32_t target_rate,
//                          bool forceStereo);

#endif
