//
// Created by t4cc0re on 3/11/21.
//
/// Included by player.h

#ifndef NATIVE_DECODER_H
#define NATIVE_DECODER_H

#include "mqa.h"
#include "types.h"
#include <libavformat/avformat.h>
#include <pthread.h>

/**
 * Enum of errors for copy_sample();
 */
enum COPY_SAMPLE_ERROR {
    SRB_CSR_NO_ERROR                = 0,
    SRB_CSR_TOO_FEW_SAMPLES         = -1,
    SRB_CSR_TARGET_BUFFER_TOO_SMALL = -2,
    SRB_CSR_INVALID_BUFFER          = -3,
    SRB_CSR_UNKNOWN                 = -4,
};

extern struct decoder_context *active_context;
extern pthread_mutex_t         active_context_lock;
extern bool                    libav_experimental;

int change_target_format(
    struct decoder_context *ctx,
    enum AVSampleFormat     target_fmt,
    u_int8_t                target_depth,
    int32_t                 target_rate,
    bool                    forceStereo);

int32_t copy_sample(srb_buffer *source, srb_buffer *target, u_int8_t sample_size);

int samples_from_frame(struct decoder_context *ctx, AVFrame *frame, srb_buffer *buffer);
int initialize_decoder_context(
    struct decoder_context *ptr,
    const char *            filename,
    int32_t                 force_rate,
    uint8_t                 force_depth,
    bool                    force_stereo,
    const char *            dither,
    const char *            resampler,
    const char *            token);

const char *stringify_context(struct decoder_context *ctx);

#endif // NATIVE_DECODER_H
