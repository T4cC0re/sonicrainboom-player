//
// Created by t4cc0re on 3/25/21.
//

#ifndef NATIVE_SOKOL_CONFIG_H
#define NATIVE_SOKOL_CONFIG_H

#include "macros.h"
#include "srb_native.h"
#include "util.h"
#include <assert.h>

#define SOKOL_ASSERT(c)    assert(c)
#define SOKOL_LOG(msg)     srb_log(mgs)
#define SOKOL_CALLOC(n, c) SRB_CALLOC(n, c)
#define SOKOL_MALLOC(s)    SRB_MALLOC(s)
#define SOKOL_FREE(p)      SRB_FREE(p)

#endif // NATIVE_SOKOL_CONFIG_H
