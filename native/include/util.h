//
// Created by t4cc0re on 3/11/21.
//

#ifndef NATIVE_UTIL_H
#define NATIVE_UTIL_H

#include "decoder.h"
#include "types.h"
#include <inttypes.h>
#include <libavutil/samplefmt.h>
#include <miniaudio.h>
#include <stdbool.h>

#if defined(__APPLE__)
// Include some more headers for `sysctlbyname()`
#    include <sys/sysctl.h>
#    include <sys/types.h>
// The headers define those. Undefine them, so we can define our own.
#    undef MIN
#    undef MAX
#endif

#include "macros.h"

enum AVAILABLE_APIS {
  #if defined(MA_HAS_NULL)
  null_device = 0,
  #endif
  #if defined(MA_HAS_CUSTOM)
  custom,
  #endif
  #if defined(MA_HAS_COREAUDIO)
  coreaudio,
  #endif
  #if defined(MA_HAS_ALSA)
  ALSA,
  #endif
  #if defined(MA_HAS_JACK)
  JACK,
  #endif
  #if defined(MA_HAS_PULSEAUDIO)
  PulseAudio,
  #endif
  #if defined(MA_HAS_WASAPI)
  WASAPI,
  #endif
  #if defined(MA_HAS_DSOUND)
  DirectSound,
  #endif
  #if defined(MA_HAS_OSS)
  OSS,
  #endif
  #if defined(MA_HAS_SNDIO)
  sndio,
  #endif
  #if defined(MA_HAS_AUDIO4)
  audio4,
  #endif
  #if defined(MA_HAS_AAUDIO)
  AAudio,
  #endif
  #if defined(MA_HAS_OPENSL)
  OpenSL,
  #endif
  #if defined(MA_HAS_WEBAUDIO)
  WebAudio,
  #endif
  END,
};


/**
 * is_apple_silicon: Check if running on Apple Silicon or Rosetta 2
 * @return
 */
static inline bool is_apple_silicon() {
#if defined(__APPLE__) && defined(__ARM64_ARCH_8__)
    return true;
#elif defined(__APPLE__) && defined(__x86_64)
    int    ret  = 0;
    size_t size = sizeof(ret);
    // Are we in rosetta? We only care about whether is becomes 1.
    (void)sysctlbyname("sysctl.proc_translated", &ret, &size, NULL, 0);

    return ret == 1;
#else
    return false;
#endif
}

static inline void srb_log(const char *msg) { errorf("srb_log: %s\n", msg); }

/**
 * get_cpu_arch: Returns CPU architecture
 *
 * If on an Apple M1 chip and include_apple_branding is true,
 * this will either return "Apple Silicon" (arm mode)
 * or "Apple Rosetta 2" (Rosetta x86 emulation).
 *
 * Otherwise possible values are:
 * arm64
 * arm7
 * arm6
 * x86_64
 * unknown (this will also generate a warning during compile time)
 *
 * @param include_apple_branding    If true, return Apple branded arch
 * @return  statically allocated const char*
 */
static inline const char *get_cpu_arch(bool include_apple_branding) {
#if defined(__x86_64)
    return include_apple_branding && is_apple_silicon() ? "Apple Rosetta 2" : "x86_64";
#elif defined(__ARM64_ARCH_8__)
    return include_apple_branding && is_apple_silicon() ? "Apple Silicon" : "arm64";
#elif defined(__ARM_ARCH_7__)
    return "arm7";
#elif defined(__ARM_ARCH_6__)
    return "arm6";
#else
#    warning "Compiling for unsupported CPU architecture"
    return "unknown";
#endif
}

static inline srb_buffer *srb_buffer_alloc(uint32_t size) {
    srb_buffer *buffer = SRB_MALLOC(sizeof(srb_buffer));
    if (buffer == NULL) { return NULL; }
    buffer->cookieA = _SRB_CONTEXT_MAGIC_A;
    buffer->self    = buffer;

    // If the caller just wanted to allocate this structure, let them.
    if (size == 0) { return buffer; }

    buffer->data = SRB_MALLOC(size);
    if (buffer->data == NULL) {
        SRB_FREE(buffer);
        return NULL;
    }

    buffer->len = size;
    return buffer;
}

static inline bool srb_buffer_ok(srb_buffer *ctx) {
    if (!ctx || ctx->cookieA != _SRB_CONTEXT_MAGIC_A || ctx->self != ctx) { return false; }

    return true;
}

/**
 * srb_buffer_realloc: ensures, the passed buffer is exactly the passed size.
 *
 * realloc with 0 size will free the underlying data buffer.
 *
 * The passed buffer must not have it's lock held by another.
 * @param buffer
 * @param size
 * @return
 */
static inline bool srb_buffer_realloc(srb_buffer *buffer, uint32_t size) {
    if (!srb_buffer_ok(buffer)) { return false; }

    pthread_mutex_lock(&buffer->lock);

    //    SRB_PRINTVAL(buffer, "p");
    //    SRB_PRINTVAL(size, "d");
    //    SRB_PRINTVAL(buffer->data, "p");
    //    SRB_PRINTVAL(buffer->len, "d");

    if (size == 0 || size != buffer->len || buffer->data == NULL) {
        SRB_FREE_NO_TRACE(buffer->data);
        buffer->len = 0;
    }

    if (size > 0) {
        buffer->data = SRB_MALLOC_NO_TRACE(size);
        if (buffer->data == NULL) {
            buffer->len = 0;

            pthread_mutex_unlock(&buffer->lock);
            return false;
        }
        buffer->len = size;
    }

    //    SRB_PRINTVAL(buffer->data, "p");
    //    SRB_PRINTVAL(buffer->len, "d");

ret:
    pthread_mutex_unlock(&buffer->lock);
    return true;
}

static inline bool context_ok(struct decoder_context *ctx) {
    if (!ctx || ctx->cookieA != _SRB_CONTEXT_MAGIC_A || ctx->cookieB != _SRB_CONTEXT_MAGIC_B) { return false; }

    return true;
}

static inline enum AVSampleFormat ma_format_to_libav(ma_format in) {
    switch (in) {
        case ma_format_u8:
            return AV_SAMPLE_FMT_U8;
        case ma_format_s16:
            return AV_SAMPLE_FMT_S16;
        case ma_format_s24:
            return AV_SAMPLE_FMT_S32;
        case ma_format_s32:
            return AV_SAMPLE_FMT_S32;
        case ma_format_f32:
            return AV_SAMPLE_FMT_FLT;
        case ma_format_unknown:
        case ma_format_count:
        default:
            return AV_SAMPLE_FMT_NONE;
    }
}

static inline const char *ma_format_string(ma_format in) {
    switch (in) {
        case ma_format_s24:
            return "s24";
        case ma_format_u8:
        case ma_format_s16:
        case ma_format_s32:
        case ma_format_f32:
            return av_get_sample_fmt_name(ma_format_to_libav(in));
        case ma_format_unknown:
        default:
            return NULL;
    }
}

static inline uint8_t bit_depth_from_ma_format(ma_format in) {
    switch (in) {
        case ma_format_u8:
            return 8;
        case ma_format_s16:
            return 16;
        case ma_format_s24:
            return 24;
        case ma_format_s32:
        case ma_format_f32:
            return 32;
        case ma_format_unknown:
        default:
            return 0;
    }
}

/**
 * @param sample_fmt    libav sample format (AV_SAMPLE_FMT_*)
 * @param depth         known bit-width for 32-bit formats
 * @return
 */
static inline uint8_t sample_bits_by_format(int32_t sample_fmt, uint8_t depth) {
    switch (sample_fmt) {
        case AV_SAMPLE_FMT_U8:
        case AV_SAMPLE_FMT_U8P:
            return 8;
        case AV_SAMPLE_FMT_DBL:
        case AV_SAMPLE_FMT_DBLP:
            return 64;
        case AV_SAMPLE_FMT_FLT:
        case AV_SAMPLE_FMT_FLTP:
            return 32;
        case AV_SAMPLE_FMT_S16:
        case AV_SAMPLE_FMT_S16P:
            return 16;
        case AV_SAMPLE_FMT_S32:
        case AV_SAMPLE_FMT_S32P:
            return (uint8_t)(depth == 24 ? 24 : 32);
        default:
            return 0;
    }
}

#endif // NATIVE_UTIL_H
