#ifndef NATIVE_MQA_H
#define NATIVE_MQA_H
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct mqa_context {
    u_int32_t originalSampleRate;
    bool      isMqa;
    bool      finished;    // Whether samples have finished analysis
    bool      isMqaStudio; // true if Mastered according to MQA Studio ("blue light")
    u_int8_t  provenance;
    u_int8_t  depth;
    uint64_t  accumulator;
};

enum ANALYZE_ERROR { ERR_NONE = 0, ERR_NO_CONTEXT, ERR_DEPTH, ERR_CHANNELS, ERR_NO_BUFF, ERR_SMALL_BUFF };

const char *        mqa_to_string(struct mqa_context *ctx);
struct mqa_context *new_mqa_context();
int                 analyze_samples(
                    struct mqa_context *context,
                    uint32_t            samples,
                    void *              buffer,
                    uint32_t            buffer_size,
                    uint8_t             depth,
                    int                 channels);

#endif // NATIVE_MQA_H
