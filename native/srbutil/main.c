#include "sokol_config.h"
#include "srb_native.h"
#include <getopt.h>

#define SOKOL_TIME_IMPL

/**
 * Aside from sokol_config.h, all sokol import must be last and after any SOKOL_* definition(s)
 */
#include "../include/decoder.h"
#include "sokol_time.h"

void benchmark_sample_copy(uint64_t iterations, uint32_t buf_size, u_int8_t sample_size) {
    srb_buffer *source      = srb_buffer_alloc(buf_size);
    uint32_t    target_size = (sample_size % 2 == 0) ? buf_size : buf_size / 4 * 3;
    srb_buffer *target      = srb_buffer_alloc(target_size);
    uint64_t    then;
    uint64_t    now;
    uint64_t    total;
    double      time;
    double      gb_s;

    memset(source->data, 0x99, source->len);
    memset(source->data + 0, 0xaa, 1);
    memset(source->data + 1, 0xab, 1);
    memset(source->data + 2, 0xac, 1);
    memset(source->data + 3, 0xad, 1);
    memset(source->data + 4, 0xba, 1);
    memset(source->data + 5, 0xbb, 1);
    memset(source->data + 6, 0xbc, 1);
    memset(source->data + 7, 0xbd, 1);
    memset(source->data + 8, 0xca, 1);
    memset(source->data + 9, 0xcb, 1);
    memset(source->data + 10, 0xcc, 1);
    memset(source->data + 11, 0xcd, 1);
    memset(source->data + 12, 0xda, 1);
    memset(source->data + 13, 0xdb, 1);
    memset(source->data + 14, 0xdc, 1);
    memset(source->data + 15, 0xdd, 1);

    then = stm_now();

    for (int i = 0; i < iterations; ++i) {
        // Use non-tracing versions in the loop to not spam the output
        srb_buffer_realloc(target, 0);
        srb_buffer_realloc(target, target_size);
        //        target = SRB_MALLOC(target_size);
        assert(copy_sample(source, target, sample_size) >= 0);
    }

    now = stm_now();

    if (sample_size == 3) {
        assert(((uint8_t *)target->data)[0] == 0xab);
        assert(((uint8_t *)target->data)[1] == 0xac);
        assert(((uint8_t *)target->data)[2] == 0xad);
        assert(((uint8_t *)target->data)[3] == 0xbb);
        assert(((uint8_t *)target->data)[4] == 0xbc);
        assert(((uint8_t *)target->data)[5] == 0xbd);
        assert(((uint8_t *)target->data)[6] == 0xcb);
        assert(((uint8_t *)target->data)[7] == 0xcc);
        assert(((uint8_t *)target->data)[8] == 0xcd);
        assert(((uint8_t *)target->data)[9] == 0xdb);
        assert(((uint8_t *)target->data)[10] == 0xdc);
        assert(((uint8_t *)target->data)[11] == 0xdd);
        assert(((uint8_t *)target->data)[12] == 0x99);
    } else {
        assert(((uint8_t *)target->data)[0] == 0xaa);
        assert(((uint8_t *)target->data)[1] == 0xab);
        assert(((uint8_t *)target->data)[2] == 0xac);
        assert(((uint8_t *)target->data)[3] == 0xad);
        assert(((uint8_t *)target->data)[4] == 0xba);
        assert(((uint8_t *)target->data)[5] == 0xbb);
        assert(((uint8_t *)target->data)[6] == 0xbc);
        assert(((uint8_t *)target->data)[7] == 0xbd);
    }

    total = (uint64_t)iterations * buf_size / 1024 / 1024 / 1024;
    time  = stm_ms(stm_diff(now, then)) / 1000;
    gb_s  = total / time;

    errorf(
        "ran %" PRIu64 " iterations of copy_sample() with sample_size %d and %" PRIu64
        " bytes each in %f sec. => %f GiB/s\n",
        iterations,
        sample_size,
        buf_size,
        time,
        gb_s);

    srb_buffer_realloc(source, 0);
    SRB_FREE(source);
    srb_buffer_realloc(target, 0);
    SRB_FREE(target);
}

void help() {
    errorf("feed me params!");
    exit(1);
}

int main(int argc, char *argv[]) {
    stm_setup();

    static struct option long_options[] = {
        {"file", required_argument, NULL, 'f'},
        {"device", required_argument, NULL, 'd'},
        {"bits", required_argument, NULL, 'b'},
        {NULL, 0, NULL, 0},
    };

    int ch;

    const char *file   = NULL;
    const char *device = NULL;
    int         bits   = 0;

    while ((ch = getopt_long(argc, argv, "f:d:b:", long_options, NULL)) != -1) {
        switch (ch) {
            case 'f':
                errorf("file: %s\n", optarg);
                file = optarg;
                break;
            case 'd':
                errorf("device: %s\n", optarg);
                device = optarg;
                break;
            case 'b':
                errorf("bits: %s\n", optarg);
                bits = atoi(optarg);
                if (bits % 8 != 0 || bits == 0) { help(); }
                break;
            default:
                exit(1);
        }
    }

    SRB_PRINTVAL(file, "s");
    SRB_PRINTVAL(device, "s");

    if (file == NULL) { help(); }

   handoff(file, 0, bits, false, NULL, NULL, device, NULL);

    print_versions();

    //    benchmark_sample_copy(1, 1024, 2);
    //    benchmark_sample_copy(1, 1024, 3);
    //    benchmark_sample_copy(1, 1024, 4);
    //    return 0;
    benchmark_sample_copy(10000000, 1024, 2);
    benchmark_sample_copy(10000000, 1024, 3);
    benchmark_sample_copy(10000000, 1024, 4);
    benchmark_sample_copy(1000000, 8192, 2);
    benchmark_sample_copy(1000000, 8192, 3);
    benchmark_sample_copy(1000000, 8192, 4);
    benchmark_sample_copy(10000000, 8192, 2);
    benchmark_sample_copy(10000000, 8192, 3);
    benchmark_sample_copy(10000000, 8192, 4);
    benchmark_sample_copy(100000, 65536, 2);
    benchmark_sample_copy(100000, 65536, 3);
    benchmark_sample_copy(100000, 65536, 4);
}
