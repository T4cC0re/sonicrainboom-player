package native

/*
	#cgo pkg-config: --static libsrb_native
	#include <srb_native.h>
*/
import "C"
import (
	"errors"
	"gitlab.com/T4cC0re/goav/avutil"
	"gitlab.com/T4cC0re/sonicrainboom-player/common"
	"gitlab.com/T4cC0re/sonicrainboom-player/log"
	"reflect"
	"unsafe"
)

var ErrNoAlloc = errors.New("could not allocate memory")

func init() {
	C.print_versions()
}

func SrbVersion() string {
	return C.GoString(C.srb_version())
}

type Native struct {
	ctx *C.struct_decoder_context
	Ctx *C.struct_external_decoder_context
}

type MQA struct {
	Ctx *C.struct_mqa_context
}

func NewMQAContext() (n MQA, err error) {
	n.Ctx = C.new_mqa_context()
	if n.Ctx == nil {
		err = ErrNoAlloc
	}
	return
}

func (n MQA) Free() {
	C.free(unsafe.Pointer(n.Ctx))
}

func (n MQA) SetFinished() {
	n.Ctx.finished = C.bool(true)
}

func (n MQA) IsMQA() bool {
	return bool(n.Ctx.isMqa)
}
func (n MQA) GetLegacyObject() common.MQAInformation {
	return common.MQAInformation{
		OriginalSampleRate: uint32(n.Ctx.originalSampleRate),
		IsMQA:              bool(n.Ctx.isMqa),
		Pending:            !(bool(n.Ctx.finished)),
		IsMQAStudio:        bool(n.Ctx.isMqaStudio),
		Provenance:         uint8(n.Ctx.provenance),
		Depth:              uint8(n.Ctx.depth),
	}
}

func (mqa MQA) AnalyzeSamples(samples uint, buffer *[]byte, depth uint8, channels int) bool {
	code := int(C.analyze_samples(mqa.Ctx, C.uint(samples), unsafe.Pointer((*reflect.SliceHeader)(unsafe.Pointer(buffer)).Data), C.uint(len(*buffer)), C.uchar(depth), C.int(channels)))

	switch code {
	case int(C.ERR_NO_CONTEXT):
		log.Error("AnalyzeSamples called without context")
	case int(C.ERR_DEPTH):
		log.Error("AnalyzeSamples called on non 16/24-bit stream")
	case int(C.ERR_CHANNELS):
		log.Error("AnalyzeSamples called on stream with other than 2 channels")
	case int(C.ERR_NO_BUFF):
		log.Error("AnalyzeSamples called without a buffer")
	case int(C.ERR_SMALL_BUFF):
		log.Error("AnalyzeSamples called small buffer")
	}

	return bool(mqa.Ctx.finished)
}

func (mqa MQA) String() (ret string) {
	char := C.mqa_to_string(mqa.Ctx)
	ret = C.GoString(char)
	C.free(unsafe.Pointer(char))
	return
}

func Handoff(
	filename string,
	forceRate int,
	forceDepth uint8,
	forceStereo bool,
	dither string,
	resampler string,
	device string,
	token string,
) {
	var tok unsafe.Pointer = nil
	if token != "" {
		tok = (unsafe.Pointer)(C.CString(token))
	}
	var dev unsafe.Pointer = nil
	if device != "" {
		dev = (unsafe.Pointer)(C.CString(device))
	}
	C.handoff(C.CString(filename), C.int(forceRate), C.uchar(forceDepth), C.bool(forceStereo), C.CString(dither), C.CString(resampler), (*C.char)(dev), (*C.char)(tok))
}

func NewContext(filename string, forceRate int, forceDepth uint8, forceStereo bool, dither string, resampler string, token string) (n Native, err error) {
	n.ctx = C.newContext()
	if n.ctx == nil {
		err = ErrNoAlloc
	}
	n.Ctx = (*C.struct_external_decoder_context)(unsafe.Pointer(n.ctx))

	var tok unsafe.Pointer = nil
	if token != "" {
		tok = (unsafe.Pointer)(C.CString(token))
	}

	ret := int(C.initialize_decoder_context(n.ctx, C.CString(filename), C.int(forceRate), C.uchar(forceDepth), C.bool(forceStereo), C.CString(dither), C.CString(resampler), (*C.char)(tok)))

	if ret < 0 {
		if ret == -1 {
			err = ErrNoAlloc
		} else {
			err = avutil.ErrorFromCode(ret)
		}
	}

	return
}

func SRBShutdown() {
	C.srb_shutdown()
}

func (n Native) String() (ret string) {
	char := C.stringify_context(n.ctx)
	ret = C.GoString(char)
	C.free(unsafe.Pointer(char))
	return
}

func (n Native) Free() {
	C.free(unsafe.Pointer(n.ctx))
}

func (n Native) GetRaw() unsafe.Pointer {
	return unsafe.Pointer(n.ctx)
}

func (n Native) ChangeTargetFormat(
	target_fmt int,
	target_depth uint8,
	target_rate int,
	forceStereo bool,
) {
	C.change_target_format(n.ctx, int32(target_fmt), C.uchar(target_depth), C.int(target_rate), C.bool(forceStereo))
}

func Connect(url string) int {
	//TODO Free string
	//return int(C.srb_connect(C.CString(url)))
	return 0
}

/**
 * ByteSliceToPtr: Parameter MUST be a pointer to a slice of any type
 */
func ByteSliceToPtr(slice *[]byte) unsafe.Pointer {
	return unsafe.Pointer(((*reflect.SliceHeader)(unsafe.Pointer(slice))).Data)
}

func FillBuffer(source []byte, offset int, target *[]byte, written int) int {
	return int(C.fill_buffer(ByteSliceToPtr(&source), C.int(len(source)), C.int(offset), ByteSliceToPtr(target), C.int(cap(*target)), C.int(written)))
}

func CopySample(src []byte, target *[]byte, samplesize uint8) int {
	buf_src := C.srb_buffer_alloc(0)
	buf_dst := C.srb_buffer_alloc(0)

	buf_src.data = ByteSliceToPtr(&src)
	buf_src.len = C.uint(len(src))
	buf_dst.data = ByteSliceToPtr(target)
	buf_dst.len = C.uint(len(*target))

	return int(
		C.copy_sample(
			buf_src,
			buf_dst,
			C.uchar(samplesize),
		),
	)
}

func (n Native) BufferFromPacket(packet unsafe.Pointer) (err error) {
	return nil
}

func (n Native) SetDitherSettings(dither int, resampler int) error {
	return errors.New("not implemented")
}
