FROM ubuntu:focal AS basebuild

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC
#RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update && apt install -y build-essential git clang-11 golang
WORKDIR /build
COPY . /build
ENV GOPROXY=direct
RUN make env && make install_pkg_deps
#RUN make dependencies/compiled
#
#FROM basebuild as native
#RUN make native
#
#FROM native
RUN make ldd
RUN ls -lsa /build/bin
