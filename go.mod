module gitlab.com/T4cC0re/sonicrainboom-player

go 1.14

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/gordonklaus/portaudio v0.0.0-20200911161147-bb74aa485641
	github.com/prometheus/common v0.14.0 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	gitlab.com/T4cC0re/goav v0.0.0-20201018025651-b970afddf66b
	gitlab.com/T4cC0re/time-track v0.0.0-20190130002920-f8a364612b66
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)

//replace gitlab.com/T4cC0re/goav => ../goav
