#!/usr/bin/env bash

dump () {
  local name="$1"
  local file="$2"
  local resampler=$3
  local depth=$4
  local rate=$5
  local dither="$6"

  local soxr_prefix="${name}.soxr.${dither}.s${depth}_${rate}_2"
  local swr_prefix="${name}.swr.${dither}.s${depth}_${rate}_2"
  local diff_prefix="${name}.diff.${dither}.s${depth}_${rate}_2"
  local tmp_prefix=""
  local OUTPUT=""

  set +x
  set -e

  for resampler in "soxr" "swr"; do
    echo "Processing '$name' with resampler $resampler at ${depth}-bit with ${rate} Hz using ${dither}-dither"

    OUTPUT="$(../sonicrainboom-player -file "${file}" -dump "/tmp/${name}.${resampler}.${dither}" -dummy -depth ${depth} -rate ${rate} -resampler "${resampler}" -dither_method "${dither}" -stereo -buffersizemultiplier 512 -buffercount 64 2>&1)"
    tmp_prefix="${name}.${resampler}.${dither}.s${depth}_${rate}_2"

    if echo "$OUTPUT" | grep -q 'dither not available' || echo "$OUTPUT" | grep -q 'Noise shaping dither on 24-bit audio is unsupported'; then
      echo "requested dither (${dither}) unavailable at ${depth}/${rate}"
      rm "/tmp/${tmp_prefix}.pcm"
      return
    fi

    ffmpeg -f "s${depth}le" -ar ${rate} -ac 2 -i "/tmp/${tmp_prefix}.pcm" -c:a copy -y "/tmp/${tmp_prefix}.wav" &>/dev/null
    rm "/tmp/${tmp_prefix}.pcm"
  done

  sox -m -v 0.5 "/tmp/${soxr_prefix}.wav" -v 0.5 "/tmp/${swr_prefix}.wav" "/tmp/${diff_prefix}.wav"
  sox "/tmp/${soxr_prefix}.wav" -n spectrogram -o "${soxr_prefix}.png"
  sox "/tmp/${swr_prefix}.wav"  -n spectrogram -o "${swr_prefix}.png"
  sox "/tmp/${diff_prefix}.wav" -n spectrogram -o "${diff_prefix}.png"
  rm "/tmp/${soxr_prefix}.wav"
  rm "/tmp/${swr_prefix}.wav"
  rm "/tmp/${diff_prefix}.wav"
}
export -f dump

dumpMass () {
  local name="$1"
  local file="$2"

  for depth in 16 24 32; do
    for rate in 44100 48000 88200 96000 192000 384000; do
      for dither in "none" "rectangular" "triangular" "triangular_hp" "shibata" "low_shibata" "high_shibata" "lipshitz" "f_weighted" "modified_e_weighted" "improved_e_weighted"; do
        echo -en "dump '$name' '$file' '$resampler' '$depth' '$rate' '$dither'\0"
      done
    done
  done
}

dumpMass "$1" "$2" | xargs -0 -rn1 -P 12 -I% bash -c "%"
